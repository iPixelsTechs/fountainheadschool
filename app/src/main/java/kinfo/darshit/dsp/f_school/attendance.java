package kinfo.darshit.dsp.f_school;

import java.io.Serializable;
import java.util.ArrayList;

import kinfo.darshit.dsp.f_school.Attendance_adapter.Students;
import kinfo.darshit.dsp.f_school.Model.Student_list;

/**
 * Created by COMP on 10-06-2017.
 */

public class attendance implements Serializable {

    String area, flag,time,r_time;
    ArrayList<Student_list.Datum> stud;
    ArrayList<Student_list.Datum> abstud;
    int areid,attendance_id;
    ArrayList<Students>present_stud;
    ArrayList<Students>ab_stud;


    public attendance(String area, String flag, ArrayList<Student_list.Datum> stud, ArrayList<Student_list.Datum> abstud) {
        this.area = area;
        this.flag = flag;
        this.stud = stud;
        this.abstud = abstud;
    }

    public attendance(int areid,String area, String time, ArrayList<Students> present_stud, ArrayList<Students> ab_stud, String flag,String r_time) {
        this.area = area;
        this.time = time;
        this.present_stud = present_stud;
        this.ab_stud = ab_stud;
        this.flag = flag;
        this.areid=areid;
        this.r_time=r_time;
    }
    /*public attendance(String area, String time, ArrayList<Students> present_stud, ArrayList<Students> ab_stud, String flag) {
        this.area = area;
        this.time = time;
        this.present_stud = present_stud;
        this.ab_stud = ab_stud;
        this.flag = flag;

    }
*/

    public int getAttendance_id() {
        return attendance_id;
    }

    public void setAttendance_id(int attendance_id) {
        this.attendance_id = attendance_id;
    }

    public String getR_time() {
        return r_time;
    }

    public void setR_time(String r_time) {
        this.r_time = r_time;
    }

    public ArrayList<Students> getPresent_stud() {
        return present_stud;
    }

    public int getAreid() {
        return areid;
    }

    public void setAreid(int areid) {
        this.areid = areid;
    }

    public void setPresent_stud(ArrayList<Students> present_stud) {
        this.present_stud = present_stud;
    }

    public ArrayList<Students> getAb_stud() {
        return ab_stud;
    }

    public void setAb_stud(ArrayList<Students> ab_stud) {
        this.ab_stud = ab_stud;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public ArrayList<Student_list.Datum> getStud() {
        return stud;
    }

    public void setStud(ArrayList<Student_list.Datum> stud) {
        this.stud = stud;
    }

    public ArrayList<Student_list.Datum> getAbstud() {
        return abstud;
    }

    public void setAbstud(ArrayList<Student_list.Datum> abstud) {
        this.abstud = abstud;
    }
}
