package kinfo.darshit.dsp.f_school.Model;

import java.io.Serializable;

/**
 * Created by COMP on 01-02-2018.
 */

public class TransportAttandanceUpdateDateTimeDetails implements Serializable{
    int attendanceId,studentId,staffId,stopId;
    String atttendaceDateTime;

    public TransportAttandanceUpdateDateTimeDetails(int attendanceId, int studentId, int staffId, int stopId, String atttendaceDateTime) {
        this.attendanceId = attendanceId;
        this.studentId = studentId;
        this.staffId = staffId;
        this.stopId = stopId;
        this.atttendaceDateTime = atttendaceDateTime;
    }

    public int getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(int attendanceId) {
        this.attendanceId = attendanceId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public String getAtttendaceDateTime() {
        return atttendaceDateTime;
    }

    public void setAtttendaceDateTime(String atttendaceDateTime) {
        this.atttendaceDateTime = atttendaceDateTime;
    }
}
