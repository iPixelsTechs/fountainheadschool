package kinfo.darshit.dsp.f_school;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/*import kinfo.darshit.dsp.f_school.Attendance_adapter.Main_adapter;*/
import kinfo.darshit.dsp.f_school.Attendance_adapter.Real_Adapter;
import kinfo.darshit.dsp.f_school.Attendance_adapter.Students;
import kinfo.darshit.dsp.f_school.Attendance_adapter.Students_2_adapter;
import kinfo.darshit.dsp.f_school.Attendance_adapter.TimeLineAdapter;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Attendance_Result;
import kinfo.darshit.dsp.f_school.Model.Bulk_stud;
import kinfo.darshit.dsp.f_school.Model.GetRoute;
import kinfo.darshit.dsp.f_school.Model.Route;
import kinfo.darshit.dsp.f_school.Model.Stud_Data;
import kinfo.darshit.dsp.f_school.Model.Student_list;
import kinfo.darshit.dsp.f_school.Model.TUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceDetail;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceMaster;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kinfo.darshit.dsp.f_school.common.url;

//G:\My_Projects\F_school
//keytool -list -v -keystore G:\My_Projects\F_school/key.jks -storepass dailydiaryatFS -alias FSAndroidKey

public class Students_2 extends AppCompatActivity   {
    ListView list;
    ArrayList<Students> student;
    ArrayList<Students> abstudent = new ArrayList<>();
    ArrayList<attendance> attend = new ArrayList<>();
    ArrayList<Student_list.Datum> real_data = new ArrayList<>();
    Students_2_adapter adapter;
    Real_Adapter ado;
    RecyclerView listing;
    Spinner sp_area, std_class, std_division;
    ArrayList<TransportAttandanceMaster> tm = new ArrayList<>();
    LinearLayout area_list;
    TextView msg;
    ArrayList<Route.Datum> data;
    ArrayList<GetRoute.Datum> routedata;
    String token = "",mobile;
    int routeid;
    boolean flag = false, area_flag = false;
    AlertDialog ad,ads;
    Button btngo,logout;
    String  send_date;
    Spinner dialog_sp, getDialog_route;
    Button btn_sub;
    ImageButton Report, sync_data;
    Boolean send_flag = false,istofs=false,live=false;
    ArrayList<String> std_name=new ArrayList<>();
    int id,data_found=0,attendance_id=0;
    ArrayList<Stud_Data> bulk_studs = new ArrayList<>();
    AutoCompleteTextView search_stud;
    ImageView img_search;
    int std_routeid;
    Database db;
    Button btn_live;
    String url=common.url,pwd;
    boolean testserver=false;
    String []allowed_number={"9898105484","9033063009","9510097292"};
    ArrayList<Students> isabsent_list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_students_2);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = sp.getString("token", "");
        mobile=sp.getString("mobile","");
        pwd=sp.getString("pwd","");
        btn_live=(Button)findViewById(R.id.btn_live); 
        area_list = (LinearLayout) findViewById(R.id.area_list);
        list = (ListView) findViewById(R.id.list);
        listing = (RecyclerView) findViewById(R.id.rec);
        sp_area = (Spinner) findViewById(R.id.area);
        std_class = (Spinner) findViewById(R.id.std_class);
        std_division = (Spinner) findViewById(R.id.std_division);

        msg = (TextView) findViewById(R.id.msg);
        btn_sub = (Button) findViewById(R.id.btn_sub);
        Report = (ImageButton) findViewById(R.id.report);
        sync_data = (ImageButton) findViewById(R.id.syn_data);
        search_stud=(AutoCompleteTextView)findViewById(R.id.Search_stud);
        img_search=(ImageView)findViewById(R.id.im_search);
        logout=(Button)findViewById(R.id.logout);

        for (int i = 0; i <allowed_number.length ; i++) {

            if (mobile.contains(allowed_number[i])) {
                    btn_live.setVisibility(View.VISIBLE);
                    testserver=true;
            }
        }

        if(testserver)
        {
            url=common.url2;
            btn_live.setText("Live");
            live=false;

        }
        else
        {
            url=common.url2;
        }

Log.e("live_url",url);


        btn_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(live)
                {
                    btn_live.setText("Test");
                    url=common.url;
                    live=false;
                    jsonlogin();
                }
                else
                {
                    btn_live.setText("Live");
                    url=common.url2;
                    live=true;
                    /*btn_live.setText("Test");
                    url=common.url;*/
                }
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor e = sp.edit();
                e.putString("token", "");
                e.clear();
                e.apply();
                e.commit();
                Intent i=new Intent(getApplication(),MainActivity.class);
                startActivity(i);
                finish();
            }
        });
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data_found==0) {
                    Toast.makeText(getApplicationContext(), "Please Download All Student data", Toast.LENGTH_LONG).show();

                } else {
                    Intent i = new Intent(getApplicationContext(), Search.class);
                    startActivityForResult(i, 2);
                }
            }
        });

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());

        Log.e("date",send_date);


        Report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Attendance_report.class);
                startActivity(i);
            }
        });

        db = new Database(getApplicationContext(), "table", null, 1);
        db.Opendb();
        Cursor cr = db.get_Stude();
        if (cr.getCount() > 0) {
            sync_data.setImageResource(R.drawable.ic_history_black_24dp);
            cr.moveToFirst();
            data_found=1;
            bulk_studs=new ArrayList<>();
            std_name=new ArrayList<>();
          /*  do{

                int id=Integer.parseInt(cr.getString(cr.getColumnIndex("sid")));
                String Name=cr.getString(cr.getColumnIndex("name"));
                String section=cr.getString(cr.getColumnIndex("sections"));
                String grade=cr.getString(cr.getColumnIndex("grade"));
                String img=cr.getString(cr.getColumnIndex("image"));
                String category=cr.getString(cr.getColumnIndex("category"));
                String EmergencyContactNo = cr.getString(cr.getColumnIndex("emergencyContactNo"));
                String FskId = cr.getString(cr.getColumnIndex("fskId"));
                String HrisEmployeeID = cr.getString(cr.getColumnIndex("hrisEmployeeID"));
                //String emergencyContactNo, String fskId, String hrisEmployeeID) {
                Stud_Data data = new Stud_Data(category, id, Name, grade, section, img,EmergencyContactNo,FskId,HrisEmployeeID,"");

                Log.e("Name",Name+"---"+category);
                if(category.equals("Student")) {
                    std_name.add(Name + " (" + grade + ")" + " section :" + section);
                }
                else
                {
                    std_name.add(Name + " (" + category + ")" );
                }
                bulk_studs.add(data);
            }while (cr.moveToNext());*/
db.close();
        }
        else {
            sync_data.setImageResource(R.drawable.ic_sync_problem_black_24dp);
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, std_name);
        search_stud.setAdapter(adapter);

        search_stud.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int selectedPos = std_name.indexOf((((TextView)view).getText()).toString());
      /*  String friend_id = selectedPos+"";
        Toast.makeText(getApplicationContext(),friend_id,Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(),std_name.get(selectedPos).toString(),Toast.LENGTH_LONG).show();
*/
        /*SomeDAO dao = getSomeDaoList().get(selectedPos);*/
        //parent.getItemAtPosition(position);
            for(int j=0;j<attend.size();j++)
            {
                    if(attend.get(j).getFlag().equals("true"))
                    {
                            ArrayList<Students> students_inrec=new ArrayList<Students>();
                            ArrayList<Students> ab_students_inrec=new ArrayList<Students>();
                            students_inrec=attend.get(j).getPresent_stud();
                            ab_students_inrec=attend.get(j).getAb_stud();
                            int stop_id=attend.get(j).getAreid();
                            String Area_name=attend.get(j).getArea();
                            String time=attend.get(j).getTime();
                        String r_time=attend.get(j).getR_time();

//Students(int id, String name, String image, String category, Boolean status, String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {
                        Log.e("Stud_name",bulk_studs.get(selectedPos).toString());
                            /*Students st = new Students(bulk_studs.get(selectedPos).getId(),
                                    bulk_studs.get(selectedPos).getName(),
                                    bulk_studs.get(selectedPos).getImage(),
                                    bulk_studs.get(selectedPos).getCategory(), false,
                                    bulk_studs.get(selectedPos).getEmergencyContactNo(),
                                    bulk_studs.get(selectedPos).getFskId(),
                                    bulk_studs.get(selectedPos).getHrisEmployeeID(),
                                    bulk_studs.get(selectedPos).getGrade(),
                                    bulk_studs.get(selectedPos).getSection());
                            students_inrec.add(st);*/
                            attendance model = new attendance(stop_id, Area_name, time,
                                    students_inrec, ab_students_inrec, "true",r_time);
                        attend.remove(j);
                            attend.add(j,model);
                            ado.notifyDataSetChanged();


                        search_stud.setText("");


                    }
            }

    }
});
        sync_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_bulk_stud();
                /*if(data_found==0)
                get_bulk_stud();
                else
                    Toast.makeText(getApplicationContext(),"Records are Already Inserted",Toast.LENGTH_LONG).show();*/
            }
        });


        btn_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("current_url",url);
                if(attend.size()>0) {
                LayoutInflater layoutInflater = (LayoutInflater) Students_2.this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                //view = layoutInflater.inflate(R.layout.main_adapter, null);
                Button submit;
                View v = layoutInflater.inflate(R.layout.submit_layout, null);
                AlertDialog.Builder builders;
                builders = new AlertDialog.Builder(Students_2.this, R.style.AppCompatAlertDialogStyle);
                builders.setView(v);
                submit=(Button)v.findViewById(R.id.btn_subs);
                ads = builders.show();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

        save_attendance();


                    //ads.dismiss();
                    }
                });

                }

            }
        });

        jsondetails();

        List<String> categories = new ArrayList<String>();
        categories.add("Citylight");
        categories.add("Adajan");
        categories.add("Katargam");
        categories.add("Nanpura");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        List<String> std_Class = new ArrayList<String>();
        std_Class.add("12");
        std_Class.add("11");
        std_Class.add("10");
        std_Class.add("9");
        // Creating adapter for spinner
        ArrayAdapter<String> stdAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, std_Class);
        stdAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        List<String> std_devision = new ArrayList<String>();
        std_devision.add("A");
        std_devision.add("B");
        std_devision.add("C");
        std_devision.add("D");
        // Creating adapter for spinner
        ArrayAdapter<String> std_devisionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, std_devision);

        // Drop down layout style - list view with radio button
        std_devisionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
     //   sp_area.setAdapter(dataAdapter);
     //   std_class.setAdapter(stdAdapter);
        std_division.setAdapter(std_devisionAdapter);


        std_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (flag) {
                    try {
                        std_routeid = routedata.get(std_class.getSelectedItemPosition()).getRouteId();
                       // routeid = routedata.get(std_class.getSelectedItemPosition()).getRouteId();
                        Log.e("routeid",std_routeid+"");
                        Json_studentlist();

                        area_list.setVisibility(View.VISIBLE);
                    } catch (Exception e) {

                        Log.e("bla bla", e.getMessage());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (area_flag) {
                    String str = data.get(sp_area.getSelectedItemPosition()).getTimeSlotId() + "";
                    routeid = 0;
                    routeid = data.get(sp_area.getSelectedItemPosition()).getTimeSlotId();
                    istofs=data.get(sp_area.getSelectedItemPosition()).getIsToFS();
                    Log.e("routeid",routeid+"");
                    jsongetroute();
                    area_list.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }



    private void Json_studentlist() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("Loading Student Data..");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        Log.e("Parse_routeid",std_routeid+"");
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);
        Call<Student_list> call = loginservice.getStudent(map, std_routeid);

        call.enqueue(new Callback<Student_list>() {
            @Override
            public void onResponse(Call<Student_list> call, Response<Student_list> response) {
               if(response.isSuccessful() && response.code()!=204) {
                   real_data = new ArrayList<>();
                   real_data = response.body().getData();
                   mProgressDialog.dismiss();
                   getstud_data();
               }
               else{
                   Toast.makeText(Students_2.this, "There are no students found", Toast.LENGTH_SHORT).show();
                   list.setAdapter(null);
               }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Student_list> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });


    }

    private void jsondetails() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("getting route data..");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);


        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);
        Call<Route> call = loginservice.GetTimeSlots(map);
        call.enqueue(new Callback<Route>() {
            @Override
            public void onResponse(Call<Route> call, Response<Route> response) {
//                Log.e("Result", response.body().getMessage().toString());
                if(response.isSuccessful() && response.code()!=204) {
                    dialog();

                    data = response.body().getData();
                    ArrayList<String> code = new ArrayList<String>();
                    for (int i = 0; i < data.size(); i++) {
                        code.add(data.get(i).getCode().trim());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Students_2.this, android.R.layout.simple_spinner_item, code);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dialog_sp.setAdapter(adapter);
                    //this Logs the correct id (:

                    sp_area.setAdapter(adapter);
                    mProgressDialog.dismiss();
                    area_flag = true;
                }
                else
                {
                    mProgressDialog.dismiss();
                    Toast.makeText(Students_2.this, "No timeslot found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Route> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });


    }

    private void jsongetroute() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("getting route data..");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);


        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);

            Log.e("parse routeid",routeid+"");
        Call<GetRoute> call = loginservice.getroute(map, routeid + "");
        call.enqueue(new Callback<GetRoute>() {
            @Override
            public void onResponse(Call<GetRoute> call, Response<GetRoute> response) {
                try {
                    if(response.isSuccessful() && response.code()!=204) {
                        Log.e("Result", response.body().getMessage());

                        routedata = response.body().getData();
                        ArrayList<String> code = new ArrayList<String>();

                        for (int i = 0; i < routedata.size(); i++) {
                            code.add(routedata.get(i).getRouteName().trim());
                        }

                        getDialog_route.setAdapter(null);
                        std_class.setAdapter(null);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Students_2.this, android.R.layout.simple_spinner_item, code);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        getDialog_route.setAdapter(adapter);
                        std_class.setAdapter(adapter);
                    }
                    else{
                    Toast.makeText(getApplicationContext(),"No route found",Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(Students_2.this, "There is no route available.", Toast.LENGTH_SHORT).show();
                    list.setAdapter(null);
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<GetRoute> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });


    }

    public void dialog() {
        View v = getLayoutInflater().inflate(R.layout.content_choose_route, null);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(Students_2.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(v);
        dialog_sp = (Spinner) v.findViewById(R.id.area);
        getDialog_route = (Spinner) v.findViewById(R.id.Route);
        btngo = (Button) v.findViewById(R.id.btn_go);
        builder.setCancelable(false);
        ad = builder.show();
        btngo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ad.dismiss();

                getstud_data();

                flag = true;

                area_list.setVisibility(View.VISIBLE);

            }
        });

        dialog_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String str = data.get(dialog_sp.getSelectedItemPosition()).getTimeSlotId() + "";
                routeid = 0;
                routeid = data.get(dialog_sp.getSelectedItemPosition()).getTimeSlotId();
                istofs=data.get(dialog_sp.getSelectedItemPosition()).getIsToFS();
                sp_area.setSelection(dialog_sp.getSelectedItemPosition());
                jsongetroute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getDialog_route.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                std_routeid = routedata.get(getDialog_route.getSelectedItemPosition()).getRouteId();

                //   Toast.makeText(getApplicationContext(),str,Toast.LENGTH_LONG).show();
                routeid = data.get(dialog_sp.getSelectedItemPosition()).getTimeSlotId();
                std_class.setSelection(getDialog_route.getSelectedItemPosition());
                Json_studentlist();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void getstud_data() {
        attend = new ArrayList<>();
        int stud_color=getResources().getColor(R.color.absent);

        if(!istofs) {
            student = new ArrayList<Students>();
            for (int i = 0; i < real_data.size(); i++) {
//public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {


                for (int j = 0; j < real_data.get(i).getPassengers().size(); j++) {
                    Students st = new Students(real_data.get(i).getPassengers().get(j).getId(),
                            real_data.get(i).getPassengers().get(j).getName(),
                            real_data.get(i).getPassengers().get(j).getImage(),
                            real_data.get(i).getPassengers().get(j).getCategory(), false,
                            real_data.get(i).getPassengers().get(j).getEmergencyContactNo(),
                            real_data.get(i).getPassengers().get(j).getFskId(),
                            real_data.get(i).getPassengers().get(j).getHrisEmployeeID(),
                            real_data.get(i).getPassengers().get(j).getGrade(),
                            real_data.get(i).getPassengers().get(j).getSection(),false,
                            real_data.get(i).getStopName(),stud_color,
                            real_data.get(i).getPassengers().get(j).getConcentflag(),
                            real_data.get(i).getStopId(),
                            real_data.get(i).getPassengers().get(j).getAbsent()

                    );
                    student.add(st);
                    if(!real_data.get(i).getPassengers().get(j).getAbsent())
                    {
                        isabsent_list.add(st);
                    }
                }

            }
            attendance model = new attendance(00, "From School", "",
                    student, abstudent, "0", "");
            attend.add(model);
        }
        else
        {

        }




        for (int i = 0; i < real_data.size(); i++) {
//public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {

            student = new ArrayList<Students>();
            isabsent_list=new ArrayList<Students>();

            for (int j = 0; j < real_data.get(i).getPassengers().size(); j++) {
                Students st = new Students(real_data.get(i).getPassengers().get(j).getId(),
                        real_data.get(i).getPassengers().get(j).getName(),
                        real_data.get(i).getPassengers().get(j).getImage(),
                        real_data.get(i).getPassengers().get(j).getCategory(), false,
                        real_data.get(i).getPassengers().get(j).getEmergencyContactNo(),
                        real_data.get(i).getPassengers().get(j).getFskId(),
                        real_data.get(i).getPassengers().get(j).getHrisEmployeeID(),
                        real_data.get(i).getPassengers().get(j).getGrade(),
                        real_data.get(i).getPassengers().get(j).getSection(),false,
                        real_data.get(i).getStopName(),stud_color,
                        real_data.get(i).getPassengers().get(j).getConcentflag(),00,
                        real_data.get(i).getPassengers().get(j).getAbsent()
                        );
                student.add(st);
                if(!real_data.get(i).getPassengers().get(j).getAbsent())
                {
                    isabsent_list.add(st);
                }


            }
            if (i == 0) {
                if(!istofs)
                {
                    attendance model = new attendance(real_data.get(i).getStopId(), real_data.get(i).getStopName(), real_data.get(i).getStopTime(),
                            student, abstudent, "2","");
                    attend.add(model);
                }
                else
                {
                    attendance model = new attendance(real_data.get(i).getStopId(), real_data.get(i).getStopName(), real_data.get(i).getStopTime(),
                            student, abstudent, "0","");
                    attend.add(model);
                }

            } else {
                attendance model = new attendance(real_data.get(i).getStopId(), real_data.get(i).getStopName(), real_data.get(i).getStopTime(),
                        student, abstudent, "2","");
                attend.add(model);
            }

        }

        ado = new Real_Adapter(Students_2.this, attend,istofs,std_routeid,token,attendance_id,url);
        list.setAdapter(ado);
    }


   /* public void send_attendance() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);




        JSONObject Main_json = new JSONObject();
        JSONArray TransportAttandanceMaster = new JSONArray();

        JSONObject master = new JSONObject();
        try {

            if(istofs) {
                for (int i = 0; i < attend.size(); i++) {

                    Log.e("areaid", attend.get(i).getAreid() + "");
                    master.put("routeId", std_routeid);
                    master.put("atttendaceDateTime", send_date);
                    JSONArray TransportAttandanceDetail = new JSONArray();
                    ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();

                    for (int j = 0; j < attend.get(i).getPresent_stud().size(); j++) {

                        JSONObject detail = new JSONObject();
                        int Stud_id = 0, staff = 0;
                        Boolean isAttend = false;
                        int stopid = attend.get(i).getAreid();

                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            detail.put("studentId", attend.get(i).getPresent_stud().get(j).getId());
                            detail.put("staffId", 0);
                            Stud_id = attend.get(i).getPresent_stud().get(j).getId();
                            staff = 0;

                        } else {
                            detail.put("studentId", 0);
                            detail.put("staffId", attend.get(i).getPresent_stud().get(j).getId());
                            staff = attend.get(i).getPresent_stud().get(j).getId();
                            Stud_id = 0;
                        }
                        detail.put("stopId", attend.get(i).getAreid());
                        detail.put("isAttend", attend.get(i).getPresent_stud().get(j).getStatus());
                        TransportAttandanceDetail cldetails = new TransportAttandanceDetail(Stud_id, staff, stopid, attend.get(i).getPresent_stud().get(j).getStatus(), attend.get(i).getR_time());
                        TransportAttandanceDetail.put(detail);
                        td.add(cldetails);

                        Log.e("child-data", Stud_id + "---" + staff + "---" + stopid + "---" + attend.get(i).getPresent_stud().get(j).getStatus() + "---" + attend.get(i).getR_time() + "-----");

                    }

                    master.put("transportAttandanceDetail", TransportAttandanceDetail);

                    TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, send_date, td);
                    tm.add(clmaster);

                }

            }
            else
            {

                for (int i = 1; i < attend.size(); i++) {

                    Log.e("areaid", attend.get(i).getAreid() + "");
                    master.put("routeId", std_routeid);
                    master.put("atttendaceDateTime", send_date);
                    JSONArray TransportAttandanceDetail = new JSONArray();
                    ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();

                    for (int j = 1; j < attend.get(i).getPresent_stud().size(); j++) {

                        JSONObject detail = new JSONObject();
                        int Stud_id = 0, staff = 0;
                        Boolean isAttend = false;
                        int stopid = attend.get(i).getAreid();

                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            detail.put("studentId", attend.get(i).getPresent_stud().get(j).getId());
                            detail.put("staffId", 0);
                            Stud_id = attend.get(i).getPresent_stud().get(j).getId();
                            staff = 0;

                        } else {
                            detail.put("studentId", 0);
                            detail.put("staffId", attend.get(i).getPresent_stud().get(j).getId());
                            staff = attend.get(i).getPresent_stud().get(j).getId();
                            Stud_id = 0;
                        }
                        detail.put("stopId", attend.get(i).getAreid());
                        detail.put("isAttend", attend.get(i).getPresent_stud().get(j).getStatus());
                        TransportAttandanceDetail cldetails = new TransportAttandanceDetail(Stud_id, staff, stopid, attend.get(i).getPresent_stud().get(j).getStatus(), attend.get(i).getR_time());
                        TransportAttandanceDetail.put(detail);
                        td.add(cldetails);

                        Log.e("child-data", Stud_id + "---" + staff + "---" + stopid + "---" + attend.get(i).getPresent_stud().get(j).getStatus() + "---" + attend.get(i).getR_time() + "-----");

                    }

                    master.put("transportAttandanceDetail", TransportAttandanceDetail);

                    TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, send_date, td);
                    tm.add(clmaster);

                }

            }
            TransportAttandanceMaster.put(master);
            Main_json.put("transportAttandanceMaster", TransportAttandanceMaster);


        } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
        }
        try {
           // send_attendance st = new send_attendance(tm);

            Log.e("sendata", Main_json.toString());
        //    Call<send_attendance> call = loginservice.send_data(map, st);

            Log.e("s_length",attend.size()+"");

            mProgressDialog.dismiss();
            Bundle extra = new Bundle();
            extra.putSerializable("data", tm);
            extra.putSerializable("absent_data", attend.get(attend.size()-1).getAb_stud());
            extra.putInt("id",id);
            Intent i =new Intent(Students_2.this,Absent_list.class);
            i.putExtra("absent_data", attend.get(attend.size()-1).getAb_stud());
            i.putExtra("data",tm);
            i.putExtra("id",id);
            startActivity(i);
            finish();
           *//* call.enqueue(new Callback<send_attendance>() {
                @Override
                public void onResponse(Call<send_attendance> call, Response<send_attendance> response) {

                    Log.e("result-code", response.message());
                    Log.e("result-code", response.body().getMessage());
                    if (response.body().getMessage().equals("success")) {
                        send_flag = true;
                        Database db = new Database(getApplicationContext(), "table", null, 1);
                        db.Opendb();
                        db.update_task(id, send_date);
                        Log.e("Send_status", send_flag + "");
                        db.close();
                    } else {
                        send_flag = false;
                    }

                    mProgressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<send_attendance> call, Throwable t) {
                    Log.e("error", t.getMessage());

                    mProgressDialog.dismiss();

                }
            });*//*
        } catch (Exception e) {
            Log.e("error------",e.getMessage().toLowerCase());
        }
        //------------------------------------------------------------------


    }*/

    private void get_bulk_stud() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("Getting Student Data");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);


        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);


        Call<Bulk_stud> call = loginservice.Insert_bulk_stud(map);
        call.enqueue(new Callback<Bulk_stud>() {
            @Override
            public void onResponse(Call<Bulk_stud> call, Response<Bulk_stud> response) {
                try {
                    Log.e("Responce code",response.code()+"");
                    if (response.code() == 200) {
                        bulk_studs=new ArrayList<Stud_Data>();
                        std_name=new ArrayList<String>();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            int id = response.body().getData().get(i).getId();
                            String name = response.body().getData().get(i).getName();
                            String grade = response.body().getData().get(i).getGrade();
                            String section = response.body().getData().get(i).getSection();
                            String img = response.body().getData().get(i).getImage();
                            String category = response.body().getData().get(i).getCategory();
                            String emergencyContactNo=response.body().getData().get(i).getEmergencyContactNo();
                            String fskid=response.body().getData().get(i).getFskId();
                            String hrisEmployeeID="";
                            hrisEmployeeID=response.body().getData().get(i).getHrisEmployeeID();
                            String StopName=response.body().getData().get(i).getStopName();




                               /* emergencyContactNo": "9925100284",
            "fskId": "FSK2008098",
            "hrisEmployeeID": null
             String emergencyContactNo, String fskId, String hrisEmployeeID) {
                                }*/
                            Stud_Data stud = new Stud_Data(category,id, name, grade, section, img,emergencyContactNo,fskid,hrisEmployeeID,StopName);
                         //   std_name.add(name+" ("+grade+")"+" section :"+section);
                            bulk_studs.add(stud);

                        }
                        inserting_stud_data();
                        mProgressDialog.dismiss();
                    }
                } catch (Exception e) {
                    Log.e("Error_bulk", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Bulk_stud> call, Throwable t) {

            }
        });


    }

    public void inserting_stud_data() {
        Database db = new Database(getApplicationContext(), "table", null, 1);
        db.Opendb();
        try {
            if (db.Delete_stud()) {
                for (int i = 0; i < bulk_studs.size(); i++) {
String hrisid="",grade="",section="",fskid="",eno="",img="",StopName="";
                    try{
                    Log.e("try_insert","ID "+bulk_studs.get(i).getId()+" Name "+ bulk_studs.get(i).getName()+
                            " Grade "+bulk_studs.get(i).getGrade()+" Section "+ bulk_studs.get(i).getSection()+
                            " Image "+bulk_studs.get(i).getImage()+" Category "+ bulk_studs.get(i).getCategory()+
                            " eno "+bulk_studs.get(i).getEmergencyContactNo().toString()+
                            " fskid "+bulk_studs.get(i).getFskId().toString()+
                            " hriseid "+bulk_studs.get(i).getHrisEmployeeID().toString());
                    hrisid=bulk_studs.get(i).getHrisEmployeeID().toString();}
                    catch(Exception e)
                    {
                        hrisid="";
                    }
                    try{
                        grade=bulk_studs.get(i).getGrade().toString();
                        section=bulk_studs.get(i).getSection().toString();
                        fskid=bulk_studs.get(i).getFskId().toString();

                    }catch(Exception e)
                    {

                    }

                    try{
                        eno=bulk_studs.get(i).getEmergencyContactNo().toString();
                    }catch(Exception e)
                    {

                    }
                    try{
                        img=bulk_studs.get(i).getImage().toString();
                    }catch(Exception e)
                    {

                    }
                    try{
                        StopName=bulk_studs.get(i).getStopName().toString();
                    }catch(Exception e)
                    {

                    }
//String emergencyContactNo,String fskId,String hrisEmployeeID) {
                    if (db.insert_bulk_stud(bulk_studs.get(i).getId(), bulk_studs.get(i).getName(),
                            grade, section,
                            img, bulk_studs.get(i).getCategory(),
                            eno,
                            fskid,hrisid,StopName
                    )) {
                        Log.e("Rec inserted", bulk_studs.get(i).getId() + "--" + bulk_studs.get(i).getName());
                    } else {
                        Log.e("Rec not inserted", "");
                    }


                }
            }
        }catch(Exception e )
        {
            Log.e("Error in insert",e.getMessage());
        }

        sync_data.setImageResource(R.drawable.ic_history_black_24dp);


            Cursor cr = db.get_Stude();
            if(cr.moveToFirst());
        {
            bulk_studs = new ArrayList<>();
            std_name = new ArrayList<>();
            do {

                /*
                  bulk_studs.get(i).getEmergencyContactNo().toString(),
                        bulk_studs.get(i).getFskId().toString(),
                        bulk_studs.get(i).getHrisEmployeeID().toString()
                */int id = Integer.parseInt(cr.getString(cr.getColumnIndex("sid")));
                String Name = cr.getString(cr.getColumnIndex("name"));
                String section = cr.getString(cr.getColumnIndex("sections"));
                String grade = cr.getString(cr.getColumnIndex("grade"));
                String img = cr.getString(cr.getColumnIndex("image"));
                String category = cr.getString(cr.getColumnIndex("category"));
                String EmergencyContactNo = cr.getString(cr.getColumnIndex("emergencyContactNo"));
                String FskId = cr.getString(cr.getColumnIndex("fskId"));
                String HrisEmployeeID = cr.getString(cr.getColumnIndex("hrisEmployeeID"));
                String StopName = cr.getString(cr.getColumnIndex("stopName"));
                //String emergencyContactNo, String fskId, String hrisEmployeeID) {
                Stud_Data data = new Stud_Data(category, id, Name, grade, section, img,EmergencyContactNo,FskId,HrisEmployeeID,StopName,"");
                Log.e("Name", Name + "---" + category);
                if (category.equals("Student")) {
                    std_name.add(Name + " (" + grade + ")" + " section :" + section);
                } else {
                    std_name.add(Name + " (" + category + ")");
                }
                bulk_studs.add(data);
            } while (cr.moveToNext());
            db.close();
            search_stud.setAdapter(null);
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, std_name);
            search_stud.setAdapter(adapter);
            data_found=1;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2) {

            try {
                Log.e("searched",data.getStringExtra("stud_img"));

                Log.e("called", "true");

                /*Students st = new Students(data.getIntExtra("stud_id", 0),
                        data.getStringExtra("stud_nm"),
                        data.getStringExtra("stud_img"),
                        data.getStringExtra("stud_category"), false,
                        data.getStringExtra("stud_eno"),
                        data.getStringExtra("stud_fskid"),
                        data.getStringExtra("stud_hemp"),
                        data.getStringExtra("stud_grade"),
                        data.getStringExtra("stud_section"));

                for (int j = 0; j < attend.size(); j++) {

                    for(int i=0;i<attend.get(j).getPresent_stud().size();i++)
                    {
                        if(attend.get(j).getPresent_stud().get(i).getId()==data.getIntExtra("stud_id", 0))
                        {

                        }
                    }

                }*/



                    for (int j = 0; j < attend.size(); j++) {

                    if (attend.get(j).getFlag().equals("0")) {

                        ArrayList<Students> students_inrec = new ArrayList<Students>();
                        ArrayList<Students> ab_students_inrec = new ArrayList<Students>();

                        students_inrec = attend.get(j).getPresent_stud();
                        ab_students_inrec = attend.get(j).getAb_stud();
                        int stop_id = attend.get(j).getAreid();
                        String Area_name = attend.get(j).getArea();
                        String time = attend.get(j).getTime();
                        String r_time="";


                        boolean isstudfound=true;

                        for(int i=0;i<students_inrec.size();i++)
                        {
                            Log.e("arr_id",students_inrec.get(i).getId()+"");
                            Log.e("search_id",data.getIntExtra("stud_id", 0)+"");

                            if(students_inrec.get(i).getId()==data.getIntExtra("stud_id", 0))
                            {
                                Log.e("arr_id",students_inrec.get(i).getId()+"");
                                Log.e("search_id",data.getIntExtra("stud_id", 0)+"");
                                isstudfound=false;
                                Toast.makeText(getApplicationContext(),"Student is Already there ",Toast.LENGTH_LONG).show();
                            }
                        }
                        for(int i=0;i<ab_students_inrec.size();i++)
                        {
                            Log.e("arr_id",ab_students_inrec.get(i).getId()+"");
                            Log.e("search_id",data.getIntExtra("stud_id", 0)+"");

                            if(ab_students_inrec.get(i).getId()==data.getIntExtra("stud_id", 0))
                            {
                                Log.e("arr_id",ab_students_inrec.get(i).getId()+"");
                                Log.e("search_id",data.getIntExtra("stud_id", 0)+"");
                                isstudfound=false;
                                Toast.makeText(getApplicationContext(),"Student is Already there ",Toast.LENGTH_LONG).show();
                            }
                        }
/*
real_data.get(i).getPassengers().get(j).getId(),
                            real_data.get(i).getPassengers().get(j).getName(),
                            real_data.get(i).getPassengers().get(j).getImage(),
                            real_data.get(i).getPassengers().get(j).getCategory(), false,
                            real_data.get(i).getPassengers().get(j).getEmergencyContactNo(),
                            real_data.get(i).getPassengers().get(j).getFskId(),
                            real_data.get(i).getPassengers().get(j).getHrisEmployeeID(),
                            real_data.get(i).getPassengers().get(j).getGrade(),
                            real_data.get(i).getPassengers().get(j).getSection(),false,
                            real_data.get(i).getStopName(),stud_color,
                            real_data.get(i).getPassengers().get(j).getConcentflag(),
                            real_data.get(i).getStopId(),
                            real_data.get(i).getPassengers().get(j).getAbsent()

 this.pre_color=pre_color;
        this.concentflag=concentflag;
        this.stopid=stopid;
        this.isAbsent=isAbsent;
*/
                        if(isstudfound) {
                            int stud_color=getResources().getColor(R.color.absent);
                            Students st = new Students(data.getIntExtra("stud_id", 0),
                                    data.getStringExtra("stud_nm"),
                                    data.getStringExtra("stud_img"),
                                    data.getStringExtra("stud_category"),
                                    false,
                                    data.getStringExtra("stud_eno"),
                                    data.getStringExtra("stud_fskid"),
                                    data.getStringExtra("stud_hemp"),
                                    data.getStringExtra("stud_grade"),
                                    data.getStringExtra("stud_section"),
                                    true,
                                    data.getStringExtra("stud_stopname"),stud_color,"true",stop_id,false);
                            students_inrec.add(st);
                            /*
                             attendance model = new attendance(00, "From School", "",
                    student, abstudent, "0", "");
                            */
                            attendance model = new attendance(stop_id, Area_name, time,
                                    students_inrec, ab_students_inrec, "0",r_time);
                            attend.remove(j);
                            attend.add(j, model);
                            ado.notifyDataSetChanged();
                            search_stud.setText("");
                        }
                    }
                }


            }catch(Exception e)
            {
                    Log.e("error-result",e.getMessage());
            }
        }

    }

    public void save_attendance() {



        for (int i = 0; i < attend.size(); i++) {
            if (attend.get(i).getFlag().equals("0") || attend.get(i).getFlag().equals("2")) {
                Toast.makeText(getApplicationContext(), "Please Complete all the routes", Toast.LENGTH_LONG).show();
                ads.dismiss();
                return;
            }
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Intent intent = new Intent(Students_2.this, Absent_list.class);
        intent.putExtra("data", attend);
        intent.putExtra("std_routeid", std_routeid);
        intent.putExtra("send_flag", send_flag);
        intent.putExtra("istofs", istofs);
        intent.putExtra("url", url);
        intent.putExtra("submit_date", send_date);
        intent.putExtra("attendance_id",attendance_id);
        Log.e("Current Url",url);
        startActivity(intent);
      //  finish();

        ads.dismiss();

        attend.clear();
        list.setAdapter(null);
        //-----------------------------------------------------------------
       /* DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        String date = df.format(Calendar.getInstance().getTime());

        db.Opendb();
        if (db.insert_master(std_routeid, date, date, send_flag, date)) {
            //   Toast.makeText(getApplicationContext(), "Rec inserted", Toast.LENGTH_LONG).show();
        } else {
            //   Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
        }
        try {
            Cursor cr = db.get_last_masterid();
            cr.moveToFirst();

            id = cr.getInt(0);
            // Toast.makeText(getApplicationContext(),""+id+"Rec Inserted",Toast.LENGTH_LONG).show();

            Log.e("attend_data", attend.size() + "");

            if(istofs) {
                for (int i = 0; i < attend.size(); i++) {
                    Log.e("Area", attend.get(i).getArea() + "");
                    Log.e("Areaid", attend.get(i).getAreid() + "");
                    Log.e("Date", date);


                    for (int j = 0; j < attend.get(i).getPresent_stud().size(); j++) {
                        Log.e("result", "--------------------------");
                        Log.e("Name", attend.get(i).getPresent_stud().get(j).getName() + "");
                        Log.e("status", attend.get(i).getPresent_stud().get(j).getStatus() + "");
                        String attend_date = attend.get(i).getR_time();
                        Log.e("category", attend.get(i).getPresent_stud().get(j).getCategory());
                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            // (int appAttandanceMasterId, int appStudentId, int appStaffId, int appStopId,Boolean isAttend,String appCreatedDateTime) {
                            if (db.insert_tblAttandanceDetail(id, attend.get(i).getPresent_stud().get(j).getId(), 0, attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        } else {
                            if (db.insert_tblAttandanceDetail(id, 0, attend.get(i).getPresent_stud().get(j).getId(), attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        }

                    }


                }
            }
            else
            {
                for (int i = 1; i < attend.size(); i++) {
                    Log.e("Area", attend.get(i).getArea() + "");
                    Log.e("Areaid", attend.get(i).getAreid() + "");
                    Log.e("Date", date);


                    for (int j = 1; j < attend.get(i).getPresent_stud().size(); j++) {
                        Log.e("result", "--------------------------");
                        Log.e("Name", attend.get(i).getPresent_stud().get(j).getName() + "");
                        Log.e("status", attend.get(i).getPresent_stud().get(j).getStatus() + "");
                        String attend_date = attend.get(i).getR_time();
                        Log.e("category", attend.get(i).getPresent_stud().get(j).getCategory());
                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            // (int appAttandanceMasterId, int appStudentId, int appStaffId, int appStopId,Boolean isAttend,String appCreatedDateTime) {
                            if (db.insert_tblAttandanceDetail(id, attend.get(i).getPresent_stud().get(j).getId(), 0, attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        } else {
                            if (db.insert_tblAttandanceDetail(id, 0, attend.get(i).getPresent_stud().get(j).getId(), attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        }

                    }


                }
            }
            Toast.makeText(Students_2.this, "Attendance Done !!", Toast.LENGTH_SHORT).show();
            area_list.setVisibility(View.GONE);

            db.close();
            send_attendance();
            ads.dismiss();

        } catch (Exception e) {
            Log.e("err_db", e.getMessage());
        }
    }*/
    }

    private void jsonlogin() {
        final ProgressDialog mProgressDialog=new ProgressDialog(Students_2.this);
        mProgressDialog.setTitle("getting data");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        /*mProgressDialog.show();
*/


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        login_model log = new login_model();
        /*log.setMobileNo("9033063009");
        log.setPassword("123456");
        */log.setMobileNo(mobile);
        log.setPassword(pwd);
        Call<login_model> call = loginservice.login(log);

        call.enqueue(new Callback<login_model>() {
            @Override
            public void onResponse(Call<login_model> call, Response<login_model> response) {
                try {
                    Log.e("responce", "------------------------------------------");
                    Log.e("responce", response.body().getMessage());
                    //     Log.e("data",response.body().getData().getAuthToken().toString() );
                    if (response.body().getMessage().equals("Success")) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Students_2.this);
                        SharedPreferences.Editor e = sp.edit();
                        e.putString("token", response.body().getData().getAuthToken());
                        e.putString("userid", response.body().getData().getUserId() + "");
                        e.putString("mobile",mobile);
                        e.putString("pwd",pwd);
                        e.commit();
                        token = response.body().getData().getAuthToken();

                        jsondetails();
                       /* Intent i = new Intent(getApplication(), Students_2.class);
                        startActivity(i);
                        Log.e("Token", response.body().getData().getAuthToken());*/
                        mProgressDialog.dismiss();
                      //  finish();
                    } else {
                       /* mProgressDialog.dismiss();*/
                        Toast.makeText(Students_2.this, "Invalid Userid or password", Toast.LENGTH_LONG).show();
                    }
                    Log.e("responce", "------------------------------------------");
                }catch(Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Wrong userid or password",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<login_model> call, Throwable t) {
                Log.e("responce", "------------------------------------------");
                Log.e("Fail", t.getMessage());
                Log.e("responce", "------------------------------------------");
                Toast.makeText(getApplicationContext(),"check your internet connection.",Toast.LENGTH_LONG).show();
               /* mProgressDialog.dismiss();*/
            }
        });

    }

}
