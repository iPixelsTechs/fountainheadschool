package kinfo.darshit.dsp.f_school.Attendance_adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import kinfo.darshit.dsp.f_school.attendance;
import kinfo.darshit.dsp.f_school.utils.VectorDrawableUtils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.ArrayList;
import java.util.List;

import kinfo.darshit.dsp.f_school.R;
import kinfo.darshit.dsp.f_school.attendance;
import kinfo.darshit.dsp.f_school.utils.VectorDrawableUtils;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.VideoInfoHolder> {

    Context context;

    ArrayList<attendance> bean;
    ArrayList<Students> present=new ArrayList<>();
    ArrayList<Students> absent=new ArrayList<>();
    Typeface fonts1,fonts2;

    public TimeLineAdapter(Context context, ArrayList<attendance> bean) {


        this.context = context;
        this.bean = bean;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

    @Override
    public VideoInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view;
        view = layoutInflater.inflate(R.layout.test,null);
        return new VideoInfoHolder(view);
    }

    @Override
    public void onBindViewHolder(final VideoInfoHolder holder,final int position) {
        holder.checked.setTag(position);
        holder.attend.setHasFixedSize(true);

        try {
            Log.e("position", position + "");
            holder.area.setText(bean.get(position).getArea().toString());
            GridViewAdapter gridViewAdapter;
            GridLayoutManager layoutManager = new GridLayoutManager(context,5);
            final GridLayoutManager absent_stud = new GridLayoutManager(context,5);
            holder.attend.setLayoutManager(layoutManager);
        //    gridViewAdapter = new GridViewAdapter(context, bean.get(position).getStud());
         //   holder.attend.setAdapter(gridViewAdapter);

            holder.absent.setLayoutManager(absent_stud);
            absent_adapter abgridViewAdapter;
          //  abgridViewAdapter = new absent_adapter(context, bean.get(position).getAbstud(),position);
        //    holder.absent.setAdapter(abgridViewAdapter);

            if(position == 0) {
                holder.mTimelineView.initLine(1);
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_inactive, ContextCompat.getColor(context,R.color.timeline)));
                //        holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker_inactive),ContextCompat.getColor(context,R.color.timeline));
            } else if (position==bean.size()-1){
                holder.mTimelineView.initLine(2);
                //  holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_active, R.color.timeline));
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker_active),ContextCompat.getColor(context,R.color.timeline));
            }
            else {
                holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker), ContextCompat.getColor(context, R.color.timeline));
            }




       /*     gridViewAdapter.setOnItemClickListener(new GridViewAdapter.ClickListener() {
                @Override
                public void onItemClick(int pos, View v) {

                    Log.e("Main_postions",position+"");
                    try {
                        LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
                        TextView name=(TextView)v.findViewById(R.id.name);
                        Toast.makeText(context,name.getText().toString(),Toast.LENGTH_LONG).show();
                     //   Students ab=new Students(holder.area.getText().toString(),name.getText().toString(),name.getText().toString(),context.getResources().getColor(R.color.white),context.getResources().getColor(R.color.white));
                    //    present.add(ab);


                        ImageView img=(ImageView)v.findViewById(R.id.img);

                        if (img.getTag().toString().equals("false")) {
                            ln_stud.setBackgroundColor(context.getResources().getColor(R.color.absent));
                            img.setTag("true");
                        } else {
                            ln_stud.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                            img.setTag("false");


                        }
                *//*TextView name=(TextView)v.findViewById(R.id.name);
                Toast.makeText(context, name.getText().toString(), Toast.LENGTH_LONG).show();*//*
                    }
                    catch(Exception e)
                    { Log.e("Error",e.getMessage());
                    //    Log.e("Click position",bean.get(position).getStud().get(pos).getStudent().toString());
                    }
                }

                @Override
                public void onItemLongClick(int position, View v) {
                    try {

                *//*TextView name=(TextView)v.findViewById(R.id.name);
                Toast.makeText(context, name.getText().toString(), Toast.LENGTH_LONG).show();*//*
                    }
                    catch(Exception e) {
                        Log.e("Error", e.getMessage());
                    }
                }
            });
*/
         /*   abgridViewAdapter.setOnItemClickListener(new absent_adapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
                    TextView name=(TextView)v.findViewById(R.id.name);
                    String pos=name.getTag().toString();
                    //  Toast.makeText(context,name.getText().toString(),Toast.LENGTH_LONG).show();
                //    Students ab=new Students(holder.area.getText().toString(),name.getText().toString(),name.getText().toString(),context.getResources().getColor(R.color.white),context.getResources().getColor(R.color.white));

                    Log.e("Current position",pos);
                    ArrayList<Students>student = new ArrayList<>();
            //        student=bean.get(Integer.parseInt(pos)).getStud();
                 //   student.add(ab);

                    for(int j = 0; j < absent.size(); j++)
                    {
                        Students obj = absent.get(j);

                        if(obj.getStudent().equals(name.getText().toString())){
                            //found, delete.
                            absent.remove(j);
                            break;
                        }

                    }

               //     attendance model = new attendance(bean.get(Integer.parseInt(pos)).getArea(), "true", student,absent);

              //      bean.set(Integer.parseInt(pos),model);
                    notifyDataSetChanged();
                }

                @Override
                public void onItemLongClick(int position, View v) {

                }
            });*/



            holder.checked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*try {

                        Log.e("length of present", present.size() + "");
                        Log.e("Length of stud", bean.get(position).getStud().size() + "");
                        Log.e("current possition", view.getTag().toString());


                        if (Integer.parseInt(view.getTag().toString()) != bean.size()) {
                            for (int i = 0; i < bean.get(Integer.parseInt(view.getTag().toString())).getStud().size(); i++) {
                                String stud = bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i).getStudent().toString();

                                Boolean matched = true;
                                for (int j = 0; j < present.size(); j++) {
                                    String pre_stud = present.get(j).getStudent().toString();
                                    if (pre_stud.equals(stud)) {
                                        matched = false;
                                    }
                                }
                                if (matched) {
                                    absent.add(bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i));
                                }
                            }

                            ArrayList<Students> student = new ArrayList<>();
                            ArrayList<Students> abdelete = new ArrayList<>();
                            attendance model;

                            student = bean.get(Integer.parseInt(view.getTag().toString())).getStud();
                            model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())).getArea(), "true", student, abdelete);

                            bean.set(Integer.parseInt(view.getTag().toString()), model);
                            notifyDataSetChanged();

                            student = bean.get(Integer.parseInt(view.getTag().toString()) + 1).getStud();
                            model = new attendance(bean.get(Integer.parseInt(view.getTag().toString()) + 1).getArea(), "true", student, absent);

                            bean.set(Integer.parseInt(view.getTag().toString()) + 1, model);
                            notifyDataSetChanged();
                            // abgridViewAdapter.notifyDataSetChanged();
                        }
                    }catch(Exception e)
                    {
                        Log.e("error_instatus", e.getMessage().toString());
                    }*/
                }
            });
    /*viewHolder.status12.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            }


    });*/


        }catch(Exception e )
        {
            Log.e("Error msg",e.getMessage());
        }


    }

    @Override
    public int getItemCount() {
        return (bean!=null? bean.size():0);
    }
    public class VideoInfoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView area;
        private RecyclerView attend,absent;

        private TimelineView mTimelineView;
        private ImageButton checked;

        public VideoInfoHolder(View itemView) {
            super(itemView);
            area=(TextView)itemView.findViewById(R.id.area);
            attend = (RecyclerView) itemView.findViewById(R.id.grid);
            absent = (RecyclerView) itemView.findViewById(R.id.absent);

            mTimelineView=(TimelineView)itemView.findViewById(R.id.time_marker);
            checked=(ImageButton)itemView.findViewById(R.id.checked);
        }

        @Override
        public void onClick(View v) {


        }
    }
}
