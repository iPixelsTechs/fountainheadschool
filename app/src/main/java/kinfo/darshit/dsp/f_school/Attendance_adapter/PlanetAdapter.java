package kinfo.darshit.dsp.f_school.Attendance_adapter;

import android.app.Activity;
import android.content.Intent;
import android.widget.Filterable;

/**
 * Created by COMP on 29-07-2017.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import kinfo.darshit.dsp.f_school.Model.Stud_Data;
import kinfo.darshit.dsp.f_school.R;
import kinfo.darshit.dsp.f_school.Students_2;

public class PlanetAdapter extends ArrayAdapter<Stud_Data> implements Filterable {

    private List<Stud_Data> planetList;
    private Context context;
    private Filter planetFilter;
    private List<Stud_Data> origPlanetList;
    private Activity act;
    public PlanetAdapter(List<Stud_Data> planetList, Context ctx,Activity act) {
        super(ctx, R.layout.list_item, planetList);
        this.planetList = planetList;
        this.context = ctx;
        this.origPlanetList = planetList;
        this.act=act;
    }

    public int getCount() {
        return planetList.size();
    }

    public Stud_Data getItem(int position) {
        return planetList.get(position);
    }

    public long getItemId(int position) {
        return planetList.get(position).hashCode();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        PlanetHolder holder = new PlanetHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item, null);
            // Now we can fill the layout with the right values
            TextView tv = (TextView) v.findViewById(R.id.name);
            ImageView distView = (ImageView) v.findViewById(R.id.img);
            LinearLayout search_stud=(LinearLayout)v.findViewById(R.id.searched_stud);


            holder.planetNameView = tv;
            holder.distView = distView;
            holder.search_stud=search_stud;
            v.setTag(holder);
        }
        else
            holder = (PlanetHolder) v.getTag();

        Stud_Data p = planetList.get(position);
        holder.planetNameView.setText(p.getStud_info());
        Glide.with(context)
                .load(p.getImage())
                .placeholder(R.drawable.fountain)
                .error(R.drawable.fountain)
                .into(holder.distView);
        //holder.distView.setText("" + p.getDistance());


        holder.search_stud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  Log.e("stud_id",planetList.get(position).getId()+"");
                Log.e("stud_nm",planetList.get(position).getName()+"");
                Log.e("stud_img",planetList.get(position).getImage()+"");
                Log.e("stud_category",planetList.get(position).getCategory()+"");
                Log.e("stud_eno",planetList.get(position).getEmergencyContactNo()+"");
                Log.e("stud_fskid",planetList.get(position).getFskId()+"");
                Log.e("stud_hemp",planetList.get(position).getHrisEmployeeID()+"");
                Log.e("stud_section",planetList.get(position).getSection()+"");*/
                Intent i =new Intent();
                i.putExtra("stud_id",planetList.get(position).getId());
                i.putExtra("stud_nm",planetList.get(position).getName());
                i.putExtra("stud_img",planetList.get(position).getImage());
                i.putExtra("stud_category",planetList.get(position).getCategory());
                i.putExtra("stud_stage",false);
                i.putExtra("stud_eno",planetList.get(position).getEmergencyContactNo());
                i.putExtra("stud_fskid",planetList.get(position).getFskId());
                i.putExtra("stud_hemp",planetList.get(position).getHrisEmployeeID());
                i.putExtra("stud_grade",planetList.get(position).getGrade());
                i.putExtra("stud_section",planetList.get(position).getSection());
                i.putExtra("stud_stopname",planetList.get(position).getStopName());

                act.setResult(2,i);
                act.finish();
            }
        });


        return v;
    }

    public void resetData() {
        planetList = origPlanetList;
    }


	/* *********************************
	 * We use the holder pattern
	 * It makes the view faster and avoid finding the component
	 * **********************************/

    private static class PlanetHolder {
        public TextView planetNameView;
        public ImageView distView;
        public LinearLayout search_stud;
    }

    // Filter Class


	/*
	 * We create our filter
	 */
    @Override
    public Filter getFilter() {
        if (planetFilter == null)
            planetFilter = new PlanetFilter();

        return planetFilter;
    }



    private class PlanetFilter extends Filter {



        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = origPlanetList;
                results.count = origPlanetList.size();
            }
            else {
                // We perform filtering operation
                List<Stud_Data> nPlanetList = new ArrayList<Stud_Data>();

                for (Stud_Data p : planetList) {
                    if (p.getStud_info().toUpperCase().contains(constraint.toString().toUpperCase()))
                        nPlanetList.add(p);
                }
                results.values = nPlanetList;
                results.count = nPlanetList.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                planetList = (List<Stud_Data>) results.values;
                notifyDataSetChanged();
            }


        }  

    }
}
