package kinfo.darshit.dsp.f_school.Attendance_adapter;

/**
 * Created by COMP on 12-06-2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kinfo.darshit.dsp.f_school.Model.Student_list;
import kinfo.darshit.dsp.f_school.R;


public class absent_adapter extends RecyclerView.Adapter<absent_adapter.ViewHolder> {
    /*private List<Students> items;*/
    private ArrayList<Students> items=new ArrayList<>();
    private Context context;
    int pos;
    private static ClickListener clickListener;
    public absent_adapter(Context context, ArrayList<Students> items,int pos) {
        this.context = context;
        for(int i=0;i<items.size();i++)
        {
            if(!items.get(i).getAbsent())
            {
                this.items.add(items.get(i));
            }
        }
        //this.items = items;
        this.pos=pos;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.attendance_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final absent_adapter.ViewHolder viewHolder, int position) {


        if(items.get(position).getAbsent())
        {
            viewHolder.ln_stud.setVisibility(View.GONE);
        }
        viewHolder.textView.setText(items.get(position).getName());
        viewHolder.textView.setTag(items.get(position).getId());
        viewHolder.ln_stud.setTag(pos);
        viewHolder.ln_stud.setBackgroundColor(items.get(position).getPre_color());
        /*try {
            Glide.with(context)
                    .load(items.get(position).getImage())
                    .placeholder(R.drawable.fountain)
                    .error(R.drawable.fountain)
                    .into(viewHolder.img);
        }catch(Exception e)
        {
            Log.e("Glid-error",e.getMessage());
        }
*/
        try {
            Glide.with(context)
                    .load(items.get(position).getImage())
                    .placeholder(R.drawable.fountain)
                    .error(R.drawable.fountain)
                    .into(viewHolder.img);
        } catch(IllegalArgumentException ex) {
            Log.wtf("Glide-tag", ex.getMessage());
        }
        viewHolder.img.setTag("false");
   //     viewHolder.img.setTag(items.get(position).getImage());
        String str="";
        try{
            str=items.get(position).getConcentflag().toString()+"";
            Log.e("getConcentflag",str);
        }catch(Exception e)
        {

        }

        try {

            if (str.equals("true"))
                viewHolder.imageView.setVisibility(View.VISIBLE);
            else
                viewHolder.imageView.setVisibility(View.GONE);
        }catch(Exception e)
        {
            viewHolder.imageView.setVisibility(View.GONE);
        }



    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * View holder to display each RecylerView item
     */
    //protected class ViewHolder extends RecyclerView.ViewHolder {
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView imageView;
        private TextView textView;
        private LinearLayout ln_stud,ln_click;
        private ImageView img;

        public ViewHolder(View view) {
            super(view);
            /*textView = (TextView)view.findViewById(R.id.text);
            imageView = (ImageView) view.findViewById(R.id.image);
            */
            ln_stud=(LinearLayout)view.findViewById(R.id.ln_stud);
            ln_click=(LinearLayout)view.findViewById(R.id.ln_click);
            textView=(TextView) view.findViewById(R.id.name);
            img=(ImageView)view.findViewById(R.id.img);
            imageView=(ImageView)view.findViewById(R.id.star);
            ln_stud.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        absent_adapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }

}
