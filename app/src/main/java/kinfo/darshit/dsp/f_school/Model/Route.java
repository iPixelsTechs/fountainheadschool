package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 04-07-2017.
 */

public class Route {

    private boolean status;

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private ArrayList<Datum> data;

    public ArrayList<Datum> getData() { return this.data; }

    public void setData(ArrayList<Datum> data) { this.data = data; }

    public Route() {
    }

    public Route(boolean status, String message, ArrayList<Datum> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public class Datum
    {
        public Datum() {
        }

        public Datum(int timeSlotId, String code, boolean isToFS, String slotTime) {
            this.timeSlotId = timeSlotId;
            this.code = code;
            this.isToFS = isToFS;
            this.slotTime = slotTime;
        }

        private int timeSlotId;

        public int getTimeSlotId() { return this.timeSlotId; }

        public void setTimeSlotId(int timeSlotId) { this.timeSlotId = timeSlotId; }

        private String code;

        public String getCode() { return this.code; }

        public void setCode(String code) { this.code = code; }

        private boolean isToFS;

        public boolean getIsToFS() { return this.isToFS; }

        public void setIsToFS(boolean isToFS) { this.isToFS = isToFS; }

        private String slotTime;

        public String getSlotTime() { return this.slotTime; }

        public void setSlotTime(String slotTime) { this.slotTime = slotTime; }
    }

}
