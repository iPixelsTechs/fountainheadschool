package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 01-02-2018.
 */

public class Attendance_Result  {
    private boolean status;

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private ArrayList<Datum> data;

    public ArrayList<Datum> getData() { return this.data; }

    public void setData(ArrayList<Datum> data) { this.data = data; }

    /*private ArrayList<object> existedAttendaceRouteId;

    public ArrayList<object> getExistedAttendaceRouteId() { return this.existedAttendaceRouteId; }

    public void setExistedAttendaceRouteId(ArrayList<object> existedAttendaceRouteId) { this.existedAttendaceRouteId = existedAttendaceRouteId; }
*/
    public class Datum
    {
        private int routeId;

        public int getRouteId() { return this.routeId; }

        public void setRouteId(int routeId) { this.routeId = routeId; }

        private int attendanceId;

        public int getAttendanceId() { return this.attendanceId; }

        public void setAttendanceId(int attendanceId) { this.attendanceId = attendanceId; }
    }

}
