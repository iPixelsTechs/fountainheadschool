package kinfo.darshit.dsp.f_school.Attendance_adapter;

import java.io.Serializable;

/**
 * Created by COMP on 10-06-2017.
 */

public class Students implements Serializable {
    String StopName,student,tag;
    int pre_color,ab_color,stopid;
    Boolean status,issearched;

    public Students() {
    }

   /* public Students(int id, String name, String image, String category,Boolean status) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.category = category;
        this.status=status;
    }*/
   public Students(int id, String name, String image, String category, Boolean status, String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section,boolean issearched,String StopName) {
       this.id = id;
       this.name = name;
       this.image = image;
       this.category = category;
       this.status = status;
       this.emergencyContactNo = emergencyContactNo;
       this.fskId = fskId;
       this.hrisEmployeeID = hrisEmployeeID;
       this.grade = grade;
       this.section = section;
       this.issearched=issearched;
       this.StopName=StopName;

   }
    public Students(int id, String name, String image, String category, Boolean status, String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section,boolean issearched,String StopName,int pre_color,String concentflag,int stopid,boolean isAbsent) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.category = category;
        this.status = status;
        this.emergencyContactNo = emergencyContactNo;
        this.fskId = fskId;
        this.hrisEmployeeID = hrisEmployeeID;
        this.grade = grade;
        this.section = section;
        this.issearched=issearched;
        this.StopName=StopName;
        this.pre_color=pre_color;
        this.concentflag=concentflag;
        this.stopid=stopid;
        this.isAbsent=isAbsent;
    }

    public int getStopid() {
        return stopid;
    }

    public void setStopid(int stopid) {
        this.stopid = stopid;
    }

    private String emergencyContactNo;

    public String getEmergencyContactNo() { return this.emergencyContactNo; }

    public void setEmergencyContactNo(String emergencyContactNo) { this.emergencyContactNo = emergencyContactNo; }

    private String fskId;

    public String getFskId() { return this.fskId; }

    public void setFskId(String fskId) { this.fskId = fskId; }

    private String hrisEmployeeID;

    public String getHrisEmployeeID() { return this.hrisEmployeeID; }

    public void setHrisEmployeeID(String hrisEmployeeID) { this.hrisEmployeeID = hrisEmployeeID; }

    private String grade;

    public String getGrade() { return this.grade; }

    public void setGrade(String grade) { this.grade = grade; }

    private String section;

    public String getSection() { return this.section; }

    public void setSection(String section) { this.section = section; }
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private String image;

    public String getImage() { return this.image; }

    public void setImage(String image) { this.image = image; }

    private String category;

    public String getCategory() { return this.category; }

    public void setCategory(String category) { this.category = category; }

    public Boolean getIssearched() {
        return issearched;
    }

    public void setIssearched(Boolean issearched) {
        this.issearched = issearched;
    }

    public String getStopName() {
        return StopName;
    }

    public void setStopName(String stopName) {
        StopName = stopName;
    }

    public int getPre_color() {
        return pre_color;
    }

    public void setPre_color(int pre_color) {
        this.pre_color = pre_color;
    }

    private String concentflag;

    public String getConcentflag() {
        return concentflag;
    }

    public void setConcentflag(String concentflag) {
        this.concentflag = concentflag;
    }

    public Boolean isAbsent;

    public Boolean getAbsent() {
        return isAbsent;
    }

    public void setAbsent(Boolean absent) {
        isAbsent = absent;
    }
}




