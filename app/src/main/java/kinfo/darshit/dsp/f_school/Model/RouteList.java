package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 07-07-2017.
 */

public class RouteList {

    private boolean status;

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private ArrayList<Datum> data;

    public ArrayList<Datum> getData() { return this.data; }

    public void setData(ArrayList<Datum> data) { this.data = data; }

    public class Passenger
    {
        private int id;

        public int getId() { return this.id; }

        public void setId(int id) { this.id = id; }

        private String name;

        public String getName() { return this.name; }

        public void setName(String name) { this.name = name; }

        private String image;

        public String getImage() { return this.image; }

        public void setImage(String image) { this.image = image; }

        private String category;

        public String getCategory() { return this.category; }

        public void setCategory(String category) { this.category = category; }
    }

    public class Datum
    {
        private int stopId;

        public int getStopId() { return this.stopId; }

        public void setStopId(int stopId) { this.stopId = stopId; }

        private String stopName;

        public String getStopName() { return this.stopName; }

        public void setStopName(String stopName) { this.stopName = stopName; }

        private int noOfPassengers;

        public int getNoOfPassengers() { return this.noOfPassengers; }

        public void setNoOfPassengers(int noOfPassengers) { this.noOfPassengers = noOfPassengers; }

        private ArrayList<Passenger> passengers;

        public ArrayList<Passenger> getPassengers() { return this.passengers; }

        public void setPassengers(ArrayList<Passenger> passengers) { this.passengers = passengers; }
    }
}
