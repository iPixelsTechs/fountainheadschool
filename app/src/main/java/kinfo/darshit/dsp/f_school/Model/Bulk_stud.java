package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 18-07-2017.
 */

public class Bulk_stud {
    private boolean status;

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private ArrayList<Datum> data;

    public ArrayList<Datum> getData() { return this.data; }

    public void setData(ArrayList<Datum> data) { this.data = data; }

    public class Datum
    {
        private int id;

        public int getId() { return this.id; }

        public void setId(int id) { this.id = id; }

        private String name;

        public String getName() { return this.name; }

        public void setName(String name) { this.name = name; }

        private String grade;

        public String getGrade() { return this.grade; }

        public void setGrade(String grade) { this.grade = grade; }

        private String section;

        public String getSection() { return this.section; }

        public void setSection(String section) { this.section = section; }

        private String image;

        public String getImage() { return this.image; }

        public void setImage(String image) { this.image = image; }

        private String category;

        public String getCategory() { return this.category; }

        public void setCategory(String category) { this.category = category; }

        private String emergencyContactNo;

        public String getEmergencyContactNo() { return this.emergencyContactNo; }

        public void setEmergencyContactNo(String emergencyContactNo) { this.emergencyContactNo = emergencyContactNo; }

        private String fskId;

        public String getFskId() { return this.fskId; }

        public void setFskId(String fskId) { this.fskId = fskId; }

        private String hrisEmployeeID;

        public String getHrisEmployeeID() { return this.hrisEmployeeID; }

        public void setHrisEmployeeID(String hrisEmployeeID) { this.hrisEmployeeID = hrisEmployeeID; }

        private String stopName;

        public String getStopName() { return this.stopName; }

        public void setStopName(String stopName) { this.stopName = stopName; }

        private Boolean concentFlag;

        public Boolean getConcentFlag() {
            return concentFlag;
        }

        public void setConcentFlag(Boolean concentFlag) {
            this.concentFlag = concentFlag;
        }
    }
}
