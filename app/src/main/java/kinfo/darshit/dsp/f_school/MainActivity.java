package kinfo.darshit.dsp.f_school;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//import kinfo.darshit.dsp.f_school.Attendance_adapter.Main_adapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    EditText edt_usr, edt_pwd;
    Button btn_login;
    String mobile,pwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       // jsonlogin();
        edt_usr = (EditText) findViewById(R.id.edt_usr);
        edt_pwd = (EditText) findViewById(R.id.edt_pwd);

        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
        /*        Intent i = new Intent(getApplication(), Students_2.class);
                startActivity(i);*/
                if(edt_usr.getText().toString().length()>0) {
                    mobile = edt_usr.getText().toString();
                }
                else {
                    edt_usr.setError("Please Enter Mobile Number");
                    return;
                }
                if(edt_pwd.getText().toString().length()>0) {
                    pwd = edt_pwd.getText().toString();
                }
                else {
                    edt_pwd.setError("Please Enter Password");
                    return;
                }
                jsonlogin();

            }
        });

  //      new Login().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void jsonlogin() {
        final ProgressDialog mProgressDialog=new ProgressDialog(MainActivity.this);
        mProgressDialog.setTitle("getting data");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(common.url2)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        login_model log = new login_model();
        /*log.setMobileNo("9033063009");
        log.setPassword("123456");
        */log.setMobileNo(mobile);
        log.setPassword(pwd);
        Call<login_model> call = loginservice.login(log);

        call.enqueue(new Callback<login_model>() {
            @Override
            public void onResponse(Call<login_model> call, Response<login_model> response) {
                try {
                    Log.e("responce", "------------------------------------------");
                    Log.e("responce", response.body().getMessage());
                    //     Log.e("data",response.body().getData().getAuthToken().toString() );
                    if (response.body().getMessage().equals("Success")) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                        SharedPreferences.Editor e = sp.edit();
                        e.putString("token", response.body().getData().getAuthToken());
                        e.putString("userid", response.body().getData().getUserId() + "");
                        e.putString("mobile",mobile);
                        e.putString("pwd",pwd);
                        e.commit();
                        Intent i = new Intent(getApplication(), Students_2.class);
                        startActivity(i);
                        finish();
                        mProgressDialog.dismiss();
                    } else {
                        mProgressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Invalid Userid or password", Toast.LENGTH_LONG).show();
                    }
                    Log.e("responce", "------------------------------------------");
                }catch(Exception e)
                {
                    mProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Wrong userid or password",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<login_model> call, Throwable t) {
                Log.e("responce", "------------------------------------------");
                Log.e("Fail", t.getMessage());
                Log.e("responce", "------------------------------------------");
                mProgressDialog.dismiss();
            }
        });

    }

}
