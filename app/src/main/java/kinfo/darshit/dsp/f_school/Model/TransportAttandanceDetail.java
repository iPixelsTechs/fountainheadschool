package kinfo.darshit.dsp.f_school.Model;

import java.io.Serializable;

/**
 * Created by COMP on 14-07-2017.
 */

public class TransportAttandanceDetail implements Serializable {


    public TransportAttandanceDetail() {
    }

    public TransportAttandanceDetail(int studentId, int staffId, int stopId, boolean isAttend,String atttendaceDateTime) {
        this.studentId = studentId;
        this.staffId = staffId;
        this.stopId = stopId;
        this.isAttend = isAttend;
        this.atttendaceDateTime=atttendaceDateTime;

    }

    public boolean isAttend() {
        return isAttend;
    }

    public void setAttend(boolean attend) {
        isAttend = attend;
    }

    private int studentId;

    public int getStudentId() { return this.studentId; }

    public void setStudentId(int studentId) { this.studentId = studentId; }

    private int staffId;

    public int getStaffId() { return this.staffId; }

    public void setStaffId(int staffId) { this.staffId = staffId; }

    private int stopId;

    public int getStopId() { return this.stopId; }

    public void setStopId(int stopId) { this.stopId = stopId; }

    private boolean isAttend;

    public boolean getIsAttend() { return this.isAttend; }

    public void setIsAttend(boolean isAttend) { this.isAttend = isAttend; }

    private String atttendaceDateTime;

    public String getAtttendaceDateTime() {
        return atttendaceDateTime;
    }

    public void setAtttendaceDateTime(String atttendaceDateTime) {
        this.atttendaceDateTime = atttendaceDateTime;
    }
}
