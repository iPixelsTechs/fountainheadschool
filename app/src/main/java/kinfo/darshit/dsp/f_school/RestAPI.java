/* JSON API for android appliation */
package kinfo.darshit.dsp.f_school;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestAPI {
  //  private final String urlString = "http://192.168.1.6/Handler1.ashx/";
 //private final String basicUrl = "http://192.168.0.226:225/";
		//http://cms.sitibuzz.com/
	//Registration
	private final String sitibuzz_api="http://transport.m-3technologies.com/api/";

    private static String convertStreamToUTF8String(InputStream stream) throws IOException {
	    String result = "";
	    StringBuilder sb = new StringBuilder();
	    try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[4096];
            int readedChars = 0;
            while (readedChars != -1) {
                readedChars = reader.read(buffer);
                if (readedChars > 0)
                   sb.append(buffer, 0, readedChars);
            }
            result = sb.toString();
		} catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


	private String load(String contents,URL url1,String token) throws IOException {
		//URL url1 = new URL(urlString);
		String result="";

		HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
		conn.setRequestMethod("POST");//Authorization
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Authorization", token);
		//conn.setRequestProperty("Accept", "application/json");
		conn.setConnectTimeout(60000);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setInstanceFollowRedirects(false);
		OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream());
		if (contents != null) {
			w.write(contents);
			w.flush();
		}

		InputStream istream = conn.getInputStream();
		result = convertStreamToUTF8String(istream);
		Log.e("Result", result);


		return result;
	}


    /*private Object mapObject(Object o) {
		Object finalValue = null;
		if (o.getClass() == String.class) {
			finalValue = o;
		}
		else if (Number.class.isInstance(o)) {
			finalValue = String.valueOf(o);
		} else if (Date.class.isInstance(o)) {
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", new Locale("en", "USA"));
			finalValue = sdf.format((Date)o);
		}
		else if (Collection.class.isInstance(o)) {
			Collection<?> col = (Collection<?>) o;
			JSONArray jarray = new JSONArray();
			for (Object item : col) {
				jarray.put(mapObject(item));
			}
			finalValue = jarray;
		} else {
			Map<String, Object> map = new HashMap<String, Object>();
			Method[] methods = o.getClass().getMethods();
			for (Method method : methods) {
				if (method.getDeclaringClass() == o.getClass()
						&& method.getModifiers() == Modifier.PUBLIC
						&& method.getName().startsWith("get")) {
					String key = method.getName().substring(3);
					try {
						Object obj = method.invoke(o, null);
						Object value = mapObject(obj);
						map.put(key, value);
						finalValue = new JSONObject(map);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}
		return finalValue;
	}*/

	public JSONObject Login(String mobileNo, String password) throws Exception {
		URL ui=new URL(sitibuzz_api+"Login");
		JSONObject result = null;
		JSONObject o = new JSONObject();
        o.put("mobileNo",mobileNo);
        o.put("password", password);
		String s = o.toString();
		Log.e("Param",s);
		String r = load(s,ui,"");
		Log.e("results",r.toString());
		result = new JSONObject(r);
		Log.e("result", result.toString());
		return result;
	}



}


