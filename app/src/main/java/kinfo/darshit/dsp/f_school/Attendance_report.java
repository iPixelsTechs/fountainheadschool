package kinfo.darshit.dsp.f_school;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import kinfo.darshit.dsp.f_school.Attendance_adapter.List_adapter;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Report_data;
import kinfo.darshit.dsp.f_school.Model.Route;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceDetail;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceMaster;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Attendance_report extends AppCompatActivity {
ListView Report_list;
    ArrayList<Report_data> attendance=new ArrayList<>();
List_adapter adapter;
String token,arrive_datetime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Report_list=(ListView)findViewById(R.id.list_item);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = sp.getString("token", "");
/*Report_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext(),"hii",Toast.LENGTH_LONG).show();
    }
});*/

        final Database db=new Database(getApplicationContext(),"table",null,1);
    //(appAttandanceMasterId ,appRouteMasterId,appAttendaceDateTime ,appCreatedDateTime ,appIsSend,appSendDateTime ";
        Cursor cr=db.get_attendance_status();
        try {
            if (cr.moveToFirst()) {
                do {
                    String date,cr_date,send_date,is_send,route,id;
                    Log.e("Date",cr.getString(cr.getColumnIndex("appAttendaceDateTime")));
                    date=cr.getString(cr.getColumnIndex("appAttendaceDateTime"));
                    Log.e("create_Date",cr.getString(cr.getColumnIndex("appCreatedDateTime")));
                    cr_date=cr.getString(cr.getColumnIndex("appCreatedDateTime"));
                    Log.e("isSend",cr.getString(cr.getColumnIndex("appIsSend")));
                    is_send=cr.getString(cr.getColumnIndex("appIsSend"));
                    Log.e("send",cr.getString(cr.getColumnIndex("appSendDateTime")));
                    send_date=cr.getString(cr.getColumnIndex("appSendDateTime"));
                    Log.e("Route",cr.getString(cr.getColumnIndex("appRouteMasterId")));
                    route=cr.getString(cr.getColumnIndex("appRouteMasterId"));
                    Log.e("id",cr.getString(cr.getColumnIndex("appAttandanceMasterId")));
                    id=cr.getString(cr.getColumnIndex("appAttandanceMasterId"));

                    // Report_data(String date, String create_Date, String date1, String route, Boolean isSend, int id) {
                    Report_data rl=new Report_data(date,cr_date,send_date,route,is_send,Integer.parseInt(id));
                    attendance.add(rl);


                    } while (cr.moveToNext());
            } else {

            }
        } catch (Exception e) {
            Log.d("Error tasklist", e.getMessage().toString());
        } finally {

        }
adapter=new List_adapter(getApplicationContext(),attendance,token,Attendance_report.this);
        Report_list.setAdapter(adapter);

Report_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
      //  Toast.makeText(getApplicationContext(),"hii",Toast.LENGTH_LONG).show();
    }
});





        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }



    public void send_attendance(final int id)
    {

        final ProgressDialog mProgressDialog=new ProgressDialog(Attendance_report.this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl(common.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice=retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy, HH:mm aaa");
        final String date = df.format(Calendar.getInstance().getTime());

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization",token);



        Database db=new Database(getApplicationContext(),"table",null,1);
        db.Opendb();

    //    Cursor cr=db.get_Student_attendance_data(id);


        ArrayList<TransportAttandanceMaster> tm=new ArrayList<TransportAttandanceMaster>();

        Cursor cr=db.get_masterid(id);
        Cursor ch;
        try {
            if (cr.moveToFirst()) {
                do {
                    int route=Integer.parseInt(cr.getString(cr.getColumnIndex("appRouteMasterId")));
                    ch=db.get_Student_attendance_data(id);
                    if(ch.moveToFirst())
                    {
                        ArrayList<TransportAttandanceDetail>td=new ArrayList<TransportAttandanceDetail>();
                        do{

                            //appStudentId Integer,appStaffId Integer,appStopId Integer,isAttend Boolean,appCreatedDateTime varchar)
                            int stud_id=Integer.parseInt(cr.getString(cr.getColumnIndex("appStudentId")));
                            int staff_id=Integer.parseInt(cr.getString(cr.getColumnIndex("appStaffId")));
                            int stop_id=Integer.parseInt(cr.getString(cr.getColumnIndex("appStopId")));
                            Boolean isAttend=Boolean.parseBoolean(cr.getString(cr.getColumnIndex("isAttend")));
                            String r_date=cr.getString(cr.getColumnIndex("appCreatedDateTime"));
                            arrive_datetime=cr.getString(cr.getColumnIndex("routeDestinationTime"));
                            TransportAttandanceDetail tadetail=new TransportAttandanceDetail(stud_id,staff_id,stop_id,isAttend,r_date);
                            td.add(tadetail);

                        }while (ch.moveToNext());
                        TransportAttandanceMaster clmaster=new TransportAttandanceMaster(route,date,arrive_datetime,td);
                        tm.add(clmaster);
                    }
                } while (cr.moveToNext());
            }
        } catch (Exception e) {
            Log.d("Error tasklist", e.getMessage().toString());
        } finally {

        }

        send_attendance st=new send_attendance(tm);

        Call<send_attendance> call = loginservice.send_data(map, st);

        call.enqueue(new Callback<send_attendance>() {
            @Override
            public void onResponse(Call<send_attendance> call, Response<send_attendance> response) {

                Log.e("result-code", response.message());
                Log.e("result-code", response.body().getMessage());
                if (response.body().getMessage().equals("success")) {
                    Database db=new Database(getApplicationContext(),"table",null,1);
                    db.Opendb();
                    db.update_task(id,date);

                } else {

                }

                mProgressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<send_attendance> call, Throwable t) {
                Log.e("error", t.getMessage());

                mProgressDialog.dismiss();

            }
        });
        adapter.notifyDataSetChanged();

    }


}
