package kinfo.darshit.dsp.f_school.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileInputStream;

/**
 * Created by Darshit on 12/30/2015.
 * emergencyContactNo": "9925100284",
 "fskId": "FSK2008098",
 "hrisEmployeeID": null
 */
public class Database extends SQLiteOpenHelper {
    public static final String tbl_master="tblAttandanceMaster";
    public static final String tbl_detail="tblAttandanceDetail";
    public static final String attend_master = "CREATE TABLE tblAttandanceMaster(appAttandanceMasterId Integer Primary key AUTOINCREMENT ,appRouteMasterId Integer,appAttendaceDateTime varchar,appCreatedDateTime varchar ,appIsSend bit,appSendDateTime varchar)";
    public static final String attend_detail="CREATE TABLE tblAttandanceDetail (appAttandanceDetailId Integer Primary key AUTOINCREMENT,appAttandanceMasterId Integer,appStudentId Integer,appStaffId Integer,appStopId Integer,isAttend Boolean,appCreatedDateTime varchar,routeDestinationTime varchar)";
    public static final String student_bulk="create table bulk_student (id Integer primary key AUTOINCREMENT," +
            "sid Integer ," +
            "name varchar," +
            "grade varchar," +
            "sections varchar," +
            "image varchar," +
            "category varchar," +
            "emergencyContactNo varchar," +
            "fskId varchar," +
            "stopName varchar,"+
            "hrisEmployeeID varchar)";
    public static final String bulk_stud="bulk_student";
    public SQLiteDatabase db;

    @Override
    public void onCreate(SQLiteDatabase db) {
        //    db.execSQL(create_emp_mast);
        try {
            db.execSQL(attend_master);
            db.execSQL(attend_detail);
            db.execSQL(student_bulk);

        }catch(Exception e)
        {
            Log.e("error in create table",e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE tblAttandanceMaster IF EXISTS");
        db.execSQL("DROP TABLE tblAttandanceDetail IF EXISTS");
        db.execSQL("DROP TABLE bulk_stud IF EXISTS");

    }

    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    //tblAttandanceMaster
//CREATE TABLE task_list(id INTEGER PRIMARY KEY AUTOINCREMENT,work VARCHAR,desc VARCHAR,sadi INTEGER,p_sadi INTEGER,rate INTEGER,dob VARCHAR)";
    public boolean insert_master(int appRouteMasterId,String appAttendaceDateTime,String appCreatedDateTime, boolean appIsSend, String appSendDateTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        cn.put("appRouteMasterId", appRouteMasterId);
        cn.put("appAttendaceDateTime", appAttendaceDateTime);
        cn.put("appCreatedDateTime", appCreatedDateTime);
        cn.put("appIsSend", appIsSend);
        cn.put("appSendDateTime", appSendDateTime);
        try {
            db.insert(tbl_master, null, cn);

        } catch (Exception en) {
            Log.d("error", en.getMessage().toString());
        } finally {
            Log.d("Message", "rec inserted");
        }
        return true;
    }
/*
"id": 14,
            "name": "Ujjwal Bansal",
            "grade": "Grade 8",
            "section": "Ardour",
            "image": "/Areas/Admin/images/StudentMale.png"

            "id": 14,
            "name": "Ujjwal Bansal",
            "grade": "Grade 8",
            "section": "Ardour",
            "image": "http://transport.m-3technologies.com/StudentAvatars/FSK2008098-16032017111856.JPG",
            "category": "Student",
            "emergencyContactNo": "9925100284",
            "fskId": "FSK2008098",
            "hrisEmployeeID": null
*/
    public boolean insert_bulk_stud(int Id,String name,String grade, String section, String image,String category,String emergencyContactNo,String fskId,String hrisEmployeeID,String stopName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        cn.put("sid", Id);
        cn.put("name", name);
        cn.put("grade", grade);
        cn.put("sections", section);
        cn.put("image", image);
        cn.put("category", category);
        cn.put("emergencyContactNo", emergencyContactNo);
        cn.put("fskId", fskId);
        //stopName
        cn.put("stopName", stopName);
        cn.put("hrisEmployeeID", hrisEmployeeID);

        try {
            db.insert(bulk_stud, null, cn);

        } catch (Exception en) {
            Log.d("error", en.getMessage().toString());
        } finally {
            Log.d("Message", "rec inserted");
        }
        return true;
    }


    public boolean insert_tblAttandanceDetail(int appAttandanceMasterId, int appStudentId, int appStaffId, int appStopId,Boolean isAttend,String appCreatedDateTime,String routeDestinationTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        //cn.put("appAttandanceDetailId", appAttandanceDetailId);
        cn.put("appAttandanceMasterId", appAttandanceMasterId);
        cn.put("appStudentId", appStudentId);
        cn.put("appStaffId", appStaffId);
        cn.put("appStopId", appStopId);
        cn.put("isAttend", isAttend);
        cn.put("appCreatedDateTime", appCreatedDateTime);
        cn.put("routeDestinationTime",routeDestinationTime);
        try {
            db.insert(tbl_detail, null, cn);

        } catch (Exception en) {
            Log.e("error", en.getMessage().toString());
        } finally {
            Log.e("Message", "rec inserted");
        }
        return true;
    }

    public Cursor get_last_masterid() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select max(appAttandanceMasterid) from tblAttandanceMaster";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }
    public Cursor get_masterid(int id) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from tblAttandanceMaster where appAttandanceMasterid="+id;
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }

    public Cursor get_Student_attendance_data(int appAttandanceMasterId) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from tblAttandanceDetail where appAttandanceMasterId="+appAttandanceMasterId;
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }

    public Cursor get_attendance_status() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from tblAttandanceMaster";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }

    public Cursor get_Stude() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from bulk_student";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }
    public Boolean update_task(int id,String date) {
        db = this.getWritableDatabase();
        Cursor cr = null;

        try {

            ContentValues cn = new ContentValues();
            cn.put("appIsSend", "1");
            cn.put("appSendDateTime", date);
            db.update(tbl_master, cn, "appAttandanceMasterId="+id, null);

        } catch (Exception e) {
            Log.e("err in login", e.getMessage().toString());
        }
        return true;

    }
    public Boolean Delete_stud() {
        db = this.getWritableDatabase();
        String str = "DELETE FROM bulk_student";
        try {

            db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in delete", e.getMessage().toString());
            return false;
        }
        return true;

    }


    //,worker_id INTEGER,work VARCHAR,desc VARCHAR,sadi INTEGER,p_sadi INTEGER,rate INTEGER,j_sadi INTEGER,dob VARCHAR,payment VARCHAR)";
    /*public Boolean update_task(int id,String work,String desc,int sadi,int p_sadi,int j_sadi,int rate,String dob,String payment) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "update task_list set work='"+work+"',desc='"+desc+"',sadi="+sadi+",p_sadi="+p_sadi+",j_sadi="+j_sadi+",rate="+rate+",dob='"+dob+"',payment='"+payment+"' where id="+id;
        try {

            ContentValues cn = new ContentValues();

            cn.put("work", work);
            cn.put("desc", desc);
            cn.put("sadi", sadi);
            cn.put("j_sadi", j_sadi);
            cn.put("p_sadi", p_sadi);
            cn.put("rate", rate);
            cn.put("dob", dob);
            cn.put("payment", payment);
            db.update(task_detail, cn, "id="+id, null);

        } catch (Exception e) {
            Log.e("err in login", e.getMessage().toString());
        }
        return true;

    }



    public boolean insert_worker(String name, String phone, String join_date) {
        byte[] byteImage1 = null;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        cn.put("name", name);
        cn.put("phone", phone);
        cn.put("join_date", join_date);

        try {

            db.insert(worker, null, cn);

        } catch (Exception en) {
            Log.d("Message", en.getMessage().toString());
        } finally {
            Log.d("Message", "rec inserted");
        }
        return true;
    }

    public boolean insert_task(String id, String task) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        cn.put("eid", id);
        cn.put("task", task);

        try {

            db.insert(task, null, cn);

        } catch (Exception en) {
            Log.d("err database", en.getMessage().toString());
        } finally {
            Log.d("rec::::::::", "inserted");
        }
        return true;
    }

    public boolean insert_task_fullinfo(String id, String task, int pripority, String fromdate, int job_code, String time, int comp_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
    *//*(id INTEGER PRIMARY KEY AUTOINCREMENT,task VARCHAR,eid INTEGER,String startdt,String enddate,String time,int priority*//*
        cn.put("eid", id);
        cn.put("task", task);
        cn.put("startdt", fromdate);
        cn.put("job_code", job_code);
        cn.put("time", time);
        cn.put("priority", pripority);
        cn.put("comp_id", comp_id);
        try {

            db.insert(task, null, cn);

        } catch (Exception en) {
            Log.d("err database", en.getMessage().toString());
        } finally {
            Log.d("rec::::::::", "inserted");
        }
        return true;
    }

    public boolean insert_company(String name) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();


        cn.put("name", name);

        try {

            db.insert(company_details, null, cn);

        } catch (Exception en) {
            Log.e("err database", en.getMessage().toString());
        } finally {
            Log.e("Comp rec::::::::", "inserted");
        }
        return true;
    }

    public boolean insert_job_code(String name) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();


        cn.put("job", name);

        try {

            db.insert(job_code, null, cn);

        } catch (Exception en) {
            Log.e("err database", en.getMessage().toString());
        } finally {
            Log.e("job rec::::::::", "inserted");
        }
        return true;
    }


    public boolean insert_task_detail(String taskid, String task_date, String ftime, String totime, String task_description, String stage) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cn = new ContentValues();
        //taskid INTEGER,date VARCHAR,task_description VARCHAR)";


        cn.put("taskid", taskid);
        cn.put("date", task_date);
        cn.put("task_description", task_description);
        cn.put("ftime", ftime);
        cn.put("totime", totime);
        cn.put("stage", stage);

        try {

            db.insert(task_detail, null, cn);

        } catch (Exception en) {
            Log.d("err database", en.getMessage().toString());
        } finally {
            Log.d("rec::::::::", "inserted");
        }
        return true;
    }

    public Cursor test_login(String nm, String pwd) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from login where name='" + nm + "'";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }


    public Cursor worker_detail() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from worker ";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }

    public Cursor work_task(int id) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from task_list where worker_id="+id;
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;

    }




    public Cursor job_code() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from job_code";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.e("err in login", e.getMessage().toString());
        }
        finally {
            if(cr.moveToFirst())
            {
                do{
                    Log.e("Record",cr.getString(cr.getColumnIndex("job")));
                }while (cr.moveToNext());
            }
            else
            {
                Log.e("No job Found","");
            }
        }
        return cr;

    }



    public Cursor task_show(String nm) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from task_mast where eid='" + nm + "'";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }

        return cr;

    }

    public Cursor task_show() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from task_mast ORDER BY priority Desc";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }

        return cr;

    }

    public Cursor task_show_mast()
    {
        db=this.getWritableDatabase();
        Cursor cr=null;
        String str="select a.*,b.*,c.* from task_mast a inner join company_detail b on a.comp_id=b.id inner join job_code c on a.job_code=c.id  ";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }
        return cr;
    }


    public Cursor task_detail(String taskid) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select b.task,a.priority,a.date,a.ftime,a.totime,a.task_description,a.stage from task_detail a inner join task_mast b\n" +
                "on a.taskid=b.id\n" +
                "where b.id='" + taskid + "' ORDER BY a.priority";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }

        return cr;
    }

    public Cursor emp_image(String nm) {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from login where id='" + nm + "'";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }

        return cr;

    }

    public Cursor emp_list() {
        db = this.getWritableDatabase();
        Cursor cr = null;
        String str = "select * from login";
        try {

            cr = db.rawQuery(str, null);

        } catch (Exception e) {
            Log.d("err in login", e.getMessage().toString());
        }

        return cr;

    }*/

    public void Opendb() {
        db = this.getWritableDatabase();
    }

    public void Closedb() {
        db.close();
    }
}


