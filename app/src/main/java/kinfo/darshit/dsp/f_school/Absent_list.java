package kinfo.darshit.dsp.f_school;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import kinfo.darshit.dsp.f_school.Attendance_adapter.GridViewAdapter;
import kinfo.darshit.dsp.f_school.Attendance_adapter.Students;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Attendance_Result;
import kinfo.darshit.dsp.f_school.Model.SetDestinationForTransportAttendance;
import kinfo.darshit.dsp.f_school.Model.TUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceDetail;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceMaster;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Absent_list extends AppCompatActivity {
RecyclerView absent;
    int tot_absent,tot_present;
    TextView tot_ab,tot_pre;
    ArrayList<Students> items=new ArrayList<>();
    ArrayList<attendance> attend = new ArrayList<>();
    ArrayList<TransportAttandanceMaster> tm=new ArrayList<>();
    Button btn;
    String  send_date,token;
    Boolean send_flag = false,istofs;
    int id,std_routeid,attendance_id;
    Database db;
    String arrive_date,url,submit_date;
    Boolean flag=false;
    ArrayList<TransportAttandanceUpdateDateTimeDetails> details=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_absent_list);
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = sp.getString("token", "");
        db = new Database(getApplicationContext(), "table", null, 1);
        attendance_id = sp.getInt("attendance_id", 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btn=(Button)findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!flag) {
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
                    arrive_date = df.format(Calendar.getInstance().getTime());
                    // send_attendance();
                    Toast.makeText(Absent_list.this, "Done!", Toast.LENGTH_SHORT).show();
                    // finish();
                    save_attendance();
                 /*   if(istofs) {
                        save_attendance();
                    }else
                    {
                        send_update();
                    }*/
                    flag=true;
                }
            }
        });
        absent=(RecyclerView)findViewById(R.id.grid);
        tot_pre=(TextView) findViewById(R.id.tot_present);
        tot_ab=(TextView) findViewById(R.id.tot_absent);
        /*items = (ArrayList<Students>)getIntent().getSerializableExtra("absent_data");
        tm= (ArrayList<TransportAttandanceMaster>)getIntent().getSerializableExtra("data");
         intent.putExtra("data",attend);
        intent.putExtra("std_routeid",std_routeid);
        intent.putExtra("send_flag",send_flag);
        intent.putExtra("istofs",istofs);*/

        attend= (ArrayList<attendance>)getIntent().getSerializableExtra("data");
        std_routeid=getIntent().getIntExtra("std_routeid",0);
        send_flag=getIntent().getBooleanExtra("send_flag",false);
        istofs=getIntent().getBooleanExtra("istofs",false);
        url=getIntent().getStringExtra("url");
        submit_date=getIntent().getStringExtra("submit_date");


        Log.e("current url",url);
        items=attend.get(attend.size()-1).getAb_stud();
        //id=getIntent().getIntExtra("id",0);
        try {
            tot_ab.setText("Total Absent Students: " + items.size());
        }catch(Exception e)
        {
            tot_ab.setText("Full Attendance");
        }


     if(items.size()>0) {
         final GridViewAdapter gridViewAdapter;
         GridLayoutManager layoutManager = new GridLayoutManager(Absent_list.this, 5);
         final GridLayoutManager absent_stud = new GridLayoutManager(Absent_list.this, 5);
         absent.setLayoutManager(layoutManager);
         gridViewAdapter = new GridViewAdapter(Absent_list.this, items, 0);
         absent.setAdapter(gridViewAdapter);
gridViewAdapter.setOnItemClickListener(new GridViewAdapter.ClickListener() {
    @Override
    public void onItemClick(int position, View v) {

    }

    @Override
    public void onItemLongClick(int position, View v) {
        //dialog(String sname, String scat, String semg, String sgrade, String ssection, String simg, String area_nm, boolean isstud_search, final int studid, final int std_position) {
        dialog(items.get(position).getName(),
                items.get(position).getCategory(),
                items.get(position).getEmergencyContactNo(),
                items.get(position).getGrade(),
                items.get(position).getSection(),
                items.get(position).getImage(),
                items.get(position).getStopName()
                );
    }
});
     }
        else
     {
         tot_pre.setText("Full Attendance");
         absent.setVisibility(View.GONE);
     }




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    public void dialog(String sname, String scat, String semg, String sgrade, String ssection, String simg, String area_nm) {
        TextView Name,category,emg,grade,section,area;
        ImageView img;

        View view;
        Button remove;
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        //view = layoutInflater.inflate(R.layout.main_adapter, null);

        View v = layoutInflater.inflate(R.layout.stud_details, null);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setView(v);
        Name = (TextView) v.findViewById(R.id.txt_nm);
        category = (TextView) v.findViewById(R.id.txt_cat);
        emg = (TextView) v.findViewById(R.id.txt_emcal);
        grade=(TextView)  v.findViewById(R.id.txt_grade);
        section=(TextView)  v.findViewById(R.id.txt_section);
        area=(TextView)  v.findViewById(R.id.txt_bus_stop);
        img=(ImageView)v.findViewById(R.id.profile_pic);
        remove=(Button)v.findViewById(R.id.btn_remove);



            remove.setVisibility(View.GONE);



        Name.setText(sname);
        category.setText(scat);
        emg.setText(semg);
        grade.setText(sgrade);
        section.setText(ssection);
        area.setText(area_nm);
        Glide.with(this)
                .load(simg)
                .placeholder(R.drawable.fountain)
                .error(R.drawable.fountain)
                .into(img);
        final AlertDialog ad = builder.show();



    }

   /* public void send_attendance() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Absent_list.this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(common.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);


       // ArrayList<TransportAttandanceMaster> tm = new ArrayList<TransportAttandanceMaster>();

        JSONObject Main_json = new JSONObject();
        JSONArray TransportAttandanceMaster = new JSONArray();

        JSONObject master = new JSONObject();

        try {
            send_attendance st = new send_attendance(tm,send_date);

            Log.e("sendata", Main_json.toString());
            Call<send_attendance> call = loginservice.send_data(map, st);

            Log.e("s_length",tm.size()+"");



            call.enqueue(new Callback<send_attendance>() {
                @Override
                public void onResponse(Call<send_attendance> call, Response<send_attendance> response) {

                    Log.e("result-code", response.message());
                    Log.e("result-code", response.body()+"");
                    if (response.body().getMessage().equals("success")) {
                        send_flag = true;
                        Database db = new Database(getApplicationContext(), "table", null, 1);
                        db.Opendb();
                        db.update_task(id, send_date);
                        Log.e("Send_status", send_flag + "");
                        db.close();


                    } else {
                        send_flag = false;
                    }

                    mProgressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<send_attendance> call, Throwable t) {
                    Log.e("error", t.getMessage());

                    mProgressDialog.dismiss();

                }
            });
        } catch (Exception e) {

        }
        //------------------------------------------------------------------


    }*/
    public void save_attendance()
    {
        for (int i = 0; i < attend.size(); i++) {
            if (attend.get(i).getFlag().equals("0") || attend.get(i).getFlag().equals("2")) {
                Toast.makeText(getApplicationContext(), "Please Complete all the routes", Toast.LENGTH_LONG).show();
                return;
            }
        }


        //-----------------------------------------------------------------
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        String date = df.format(Calendar.getInstance().getTime());
//(int appRouteMasterId,String appAttendaceDateTime,String appCreatedDateTime, boolean appIsSend, String appSendDateTime) {
        db.Opendb();
        if (db.insert_master(std_routeid, submit_date, date, send_flag, date)) {
            //   Toast.makeText(getApplicationContext(), "Rec inserted", Toast.LENGTH_LONG).show();
        } else {
            //   Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
        }
        try {
            Cursor cr = db.get_last_masterid();
            cr.moveToFirst();

            id = cr.getInt(0);
            // Toast.makeText(getApplicationContext(),""+id+"Rec Inserted",Toast.LENGTH_LONG).show();

            Log.e("attend_data", attend.size() + "");

            if(istofs) {
                for (int i = 0; i < attend.size(); i++) {
                    Log.e("Area", attend.get(i).getArea() + "");
                    Log.e("Areaid", attend.get(i).getAreid() + "");
                    Log.e("Date", date);


                    for (int j = 0; j < attend.get(i).getPresent_stud().size(); j++) {
                        Log.e("result", "--------------------------");
                        Log.e("Name", attend.get(i).getPresent_stud().get(j).getName() + "");
                        Log.e("status", attend.get(i).getPresent_stud().get(j).getStatus() + "");
                        String attend_date = attend.get(i).getR_time();
                        Log.e("category", attend.get(i).getPresent_stud().get(j).getCategory());
                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            // (int appAttandanceMasterId, int appStudentId, int appStaffId, int appStopId,Boolean isAttend,String appCreatedDateTime) {
                            if (db.insert_tblAttandanceDetail(id, attend.get(i).getPresent_stud().get(j).getId(), 0, attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date,arrive_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        } else {
                            if (db.insert_tblAttandanceDetail(id, 0, attend.get(i).getPresent_stud().get(j).getId(), attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date,arrive_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        }

                    }


                }
            }
            else
            {
                for (int i = 1; i < attend.size(); i++) {
                    Log.e("Area", attend.get(i).getArea() + "");
                    Log.e("Areaid", attend.get(i).getAreid() + "");
                    Log.e("Date", date);


                    for (int j = 1; j < attend.get(i).getPresent_stud().size(); j++) {
                        Log.e("result", "--------------------------");
                        Log.e("Name", attend.get(i).getPresent_stud().get(j).getName() + "");
                        Log.e("status", attend.get(i).getPresent_stud().get(j).getStatus() + "");
                        String attend_date = attend.get(i).getR_time();
                        Log.e("category", attend.get(i).getPresent_stud().get(j).getCategory());
                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            // (int appAttandanceMasterId, int appStudentId, int appStaffId, int appStopId,Boolean isAttend,String appCreatedDateTime) {
                            if (db.insert_tblAttandanceDetail(id, attend.get(i).getPresent_stud().get(j).getId(), 0, attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date,arrive_date)) {
                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        } else {
                            if (db.insert_tblAttandanceDetail(id, 0, attend.get(i).getPresent_stud().get(j).getId(), attend.get(i).getAreid(), attend.get(i).getPresent_stud().get(j).getStatus(), attend_date,arrive_date)) {                                Log.e("Inserted Recid", attend.get(i).getPresent_stud().get(j).getName());
                            }
                        }

                    }


                }
            }
            Toast.makeText(Absent_list.this, "Attendance Done !!", Toast.LENGTH_SHORT).show();
           // area_list.setVisibility(View.GONE);

            db.close();
            if(istofs) {
                send_attendance();
            }else
            {
                send_update();
            }
        //    ads.dismiss();

        } catch (Exception e) {
            Log.e("err_db", e.getMessage());
        }
    }
    public void send_attendance() {
        final ProgressDialog mProgressDialog = new ProgressDialog(Absent_list.this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);




        JSONObject Main_json = new JSONObject();
        JSONArray TransportAttandanceMaster = new JSONArray();

        JSONObject master = new JSONObject();
        try {
            ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();
            if(istofs) {
                for (int i = 0; i < attend.size(); i++) {

                    Log.e("areaid", attend.get(i).getAreid() + "");
                    master.put("routeId", std_routeid);
                    master.put("atttendaceDateTime", send_date);
                    JSONArray TransportAttandanceDetail = new JSONArray();


                    for (int j = 0; j < attend.get(i).getPresent_stud().size(); j++) {

                        JSONObject detail = new JSONObject();
                        int Stud_id = 0, staff = 0;
                        Boolean isAttend = false;
                        int stopid = attend.get(i).getAreid();

                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            detail.put("studentId", attend.get(i).getPresent_stud().get(j).getId());
                            detail.put("staffId", 0);
                            Stud_id = attend.get(i).getPresent_stud().get(j).getId();
                            staff = 0;

                        } else {
                            detail.put("studentId", 0);
                            detail.put("staffId", attend.get(i).getPresent_stud().get(j).getId());
                            staff = attend.get(i).getPresent_stud().get(j).getId();
                            Stud_id = 0;
                        }
                        detail.put("stopId", attend.get(i).getAreid());
                        detail.put("isAttend", attend.get(i).getPresent_stud().get(j).getStatus());
                        TransportAttandanceDetail cldetails = new TransportAttandanceDetail(Stud_id, staff, stopid, attend.get(i).getPresent_stud().get(j).getStatus(), attend.get(i).getR_time());
                        TransportAttandanceDetail.put(detail);
                        td.add(cldetails);

                        Log.e("child-data", Stud_id + "---" + staff + "---" + stopid + "---" + attend.get(i).getPresent_stud().get(j).getStatus() + "---" + attend.get(i).getR_time() + "-----");

                    }

                    master.put("transportAttandanceDetail", TransportAttandanceDetail);

                    /*TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, send_date, arrive_date,td);
                    tm.add(clmaster);*/

                }

            }
            else
            {

                for (int i = 1; i < attend.size(); i++) {

                    Log.e("areaid", attend.get(i).getAreid() + "");
                    master.put("routeId", std_routeid);
                    master.put("atttendaceDateTime", send_date);
                    JSONArray TransportAttandanceDetail = new JSONArray();
                    //ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();

                    for (int j = 1; j < attend.get(i).getPresent_stud().size(); j++) {

                        JSONObject detail = new JSONObject();
                        int Stud_id = 0, staff = 0;
                        Boolean isAttend = false;
                        int stopid = attend.get(i).getAreid();

                        if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                            detail.put("studentId", attend.get(i).getPresent_stud().get(j).getId());
                            detail.put("staffId", 0);
                            Stud_id = attend.get(i).getPresent_stud().get(j).getId();
                            staff = 0;

                        } else {
                            detail.put("studentId", 0);
                            detail.put("staffId", attend.get(i).getPresent_stud().get(j).getId());
                            staff = attend.get(i).getPresent_stud().get(j).getId();
                            Stud_id = 0;
                        }
                        detail.put("stopId", attend.get(i).getAreid());
                        detail.put("isAttend", attend.get(i).getPresent_stud().get(j).getStatus());
                        TransportAttandanceDetail cldetails = new TransportAttandanceDetail(Stud_id, staff, stopid, attend.get(i).getPresent_stud().get(j).getStatus(), attend.get(i).getR_time());
                        TransportAttandanceDetail.put(detail);
                        td.add(cldetails);

                        Log.e("child-data", Stud_id + "---" + staff + "---" + stopid + "---" + attend.get(i).getPresent_stud().get(j).getStatus() + "---" + attend.get(i).getR_time() + "-----");

                    }

                    master.put("transportAttandanceDetail", TransportAttandanceDetail);

                    /*TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, send_date,arrive_date, td);
                    tm.add(clmaster);*/

                }

            }
            TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, submit_date, arrive_date,td);
            tm.add(clmaster);
            TransportAttandanceMaster.put(master);
            Main_json.put("transportAttandanceMaster", TransportAttandanceMaster);


        } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
        }
        try {
            send_attendance st = new send_attendance(tm);

            Log.e("sendata", Main_json.toString());
            Call<send_attendance> call = loginservice.send_data(map, st);

            Log.e("s_length",attend.size()+"");

             call.enqueue(new Callback<send_attendance>() {
                @Override
                public void onResponse(Call<send_attendance> call, Response<send_attendance> response) {

                    Log.e("result-code", response.message());
                    Log.e("result-code", response.body().getMessage());
                    if (response.body().getMessage().equals("success")) {
                        send_flag = true;
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
                        String date = df.format(Calendar.getInstance().getTime());
                        Database db = new Database(getApplicationContext(), "table", null, 1);
                        db.Opendb();
                        db.update_task(id, date);
                        Log.e("Send_status", send_flag + "");
                        db.close();
                        dialog();


                    } else {
                        send_flag = false;
                    }

                    mProgressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<send_attendance> call, Throwable t) {
                    Log.e("error", t.getMessage());

                    mProgressDialog.dismiss();

                }
            });
        } catch (Exception e) {
            Log.e("error------",e.getMessage().toLowerCase());
        }
        //------------------------------------------------------------------


    }



    public void dialog() {
        Button btn_save;
        View v = getLayoutInflater().inflate(R.layout.attendance_done, null);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(Absent_list.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(v);
        btn_save = (Button) v.findViewById(R.id.btn_save);
        builder.setCancelable(false);
        AlertDialog ad;
        ad = builder.show();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


    }
    public void send_update() {
        final ProgressDialog mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);

        JSONObject master = new JSONObject();
        try {

            //ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();
        for(int i=0;i<attend.size();i++) {
            ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();
            Log.e("areaid", attend.get(i).getAreid() + "");
            send_date=attend.get(i).getR_time();
            master.put("routeId", std_routeid);
            master.put("atttendaceDateTime", submit_date);

            for (int j = 0; j < attend.get(i).getPresent_stud().size(); j++) {
                int stud_id = 0, staff_id = 0;
                //(int attendanceId, int studentId, int staffId, int stopId, String atttendaceDateTime) {
                if (attend.get(i).getPresent_stud().get(j).getCategory().equals("Student")) {
                    stud_id = attend.get(i).getPresent_stud().get(j).getId();
                } else {
                    staff_id = attend.get(i).getPresent_stud().get(j).getId();
                }
                details.add(new TransportAttandanceUpdateDateTimeDetails(attendance_id, stud_id, staff_id, attend.get(i).getAreid(), send_date));
            }
        }
            TUpdateDateTimeDetails tdt=new TUpdateDateTimeDetails();
            tdt.setTransportAttandanceUpdateDateTimeDetails(details);

            Call<Attendance_Result> call = loginservice.send_data_istofs2(map, tdt);

            Log.e("s_length",attend+"");
            call.enqueue(new Callback<Attendance_Result>() {
                @Override
                public void onResponse(Call<Attendance_Result> call, Response<Attendance_Result> response) {
                    if(response.isSuccessful())
                    {
                        Log.e("status","Data passed");
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
                        String date = df.format(Calendar.getInstance().getTime());
                        Database db = new Database(getApplicationContext(), "table", null, 1);
                        db.Opendb();
                        db.update_task(id, date);
                        Log.e("Send_status", send_flag + "");
                        db.close();

                        SetDestination();
                    }
                    else
                    {
                        Log.e("status",response.code()+"");
                    }
                    mProgressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Attendance_Result> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });




        } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
            mProgressDialog.dismiss();
        }

        //------------------------------------------------------------------

    }

    public void SetDestination() {
        final ProgressDialog mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());


        try {
            SetDestinationForTransportAttendance tdt=new SetDestinationForTransportAttendance();
            tdt.setAttendanceId(attendance_id);
            tdt.setRouteDestinationTime(send_date);

            Call<SetDestinationForTransportAttendance> call = loginservice.setDestination_transport(map, tdt);
            Log.e("s_length",attend+"");
            call.enqueue(new Callback<SetDestinationForTransportAttendance>() {
             @Override
             public void onResponse(Call<SetDestinationForTransportAttendance> call, Response<SetDestinationForTransportAttendance> response) {
                 if(response.isSuccessful())
                 {
                     Log.e("Response","Successful data passed ");
                     dialog();
                 }
                 Log.e("Response-code",response.code()+"");
             }

             @Override
             public void onFailure(Call<SetDestinationForTransportAttendance> call, Throwable t) {

             }
         });




        } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
            mProgressDialog.dismiss();
        }

        //------------------------------------------------------------------

    }
}
