package kinfo.darshit.dsp.f_school.Model;

/**
 * Created by COMP on 18-07-2017.
 */

public class Stud_Data {

    private String stud_info;

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private String grade;

    public String getGrade() { return this.grade; }

    public void setGrade(String grade) { this.grade = grade; }

    private String section;

    public String getSection() { return this.section; }

    public void setSection(String section) { this.section = section; }

    private String image;

    public String getImage() { return this.image; }

    public void setImage(String image) { this.image = image; }

    public Boolean concentFlag;

    public Boolean getConcentFlag() {
        return concentFlag;
    }

    public void setConcentFlag(Boolean concentFlag) {
        this.concentFlag = concentFlag;
    }

    /*
        public Stud_Data(String category, int id, String name, String grade, String section, String image) {
            this.category = category;
            this.id = id;
            this.name = name;
            this.grade = grade;
            this.section = section;
            this.image = image;
        }
    */
private String stopName;

    public String getStopName() { return this.stopName; }

    public void setStopName(String stopName) { this.stopName = stopName; }

    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Stud_Data(String category, int id, String name, String grade, String section, String image, String emergencyContactNo, String fskId, String hrisEmployeeID, String stopName) {
        this.category = category;
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.section = section;
        this.image = image;
        this.emergencyContactNo = emergencyContactNo;
        this.fskId = fskId;
        this.hrisEmployeeID = hrisEmployeeID;
        this.stopName=stopName;

    }

    public Stud_Data(String category, int id, String name, String grade, String section, String image, String emergencyContactNo, String fskId, String hrisEmployeeID, String stud_info,String stopName) {
        this.category = category;
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.section = section;
        this.image = image;
        this.emergencyContactNo = emergencyContactNo;
        this.fskId = fskId;
        this.hrisEmployeeID = hrisEmployeeID;
        this.stud_info = stud_info;
        this.stopName=stopName;


    }

    public String getStud_info() {
        return stud_info;
    }

    public void setStud_info(String stud_info) {
        this.stud_info = stud_info;
    }

    private String emergencyContactNo;

    public String getEmergencyContactNo() { return this.emergencyContactNo; }

    public void setEmergencyContactNo(String emergencyContactNo) { this.emergencyContactNo = emergencyContactNo; }

    private String fskId;

    public String getFskId() { return this.fskId; }

    public void setFskId(String fskId) { this.fskId = fskId; }

    private String hrisEmployeeID;

    public String getHrisEmployeeID() { return this.hrisEmployeeID; }

    public void setHrisEmployeeID(String hrisEmployeeID) { this.hrisEmployeeID = hrisEmployeeID; }



}
