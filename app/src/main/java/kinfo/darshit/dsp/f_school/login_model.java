package kinfo.darshit.dsp.f_school;


import android.provider.ContactsContract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by COMP on 28-03-2017.
 */

public class login_model {

    public login_model() {
    }

    @Expose @SerializedName("status")

    private String status ;
    @Expose @SerializedName("message")
    private String message;
    @Expose @SerializedName("data")
    private data data;

    public login_model(String status, String message,data data, String mobileNo, String password) {
        this.status = status;
        this.message = message;
        this.mobileNo = mobileNo;
        this.data=data;
        Password = password;

    }

    public login_model.data getData() {
        return data;
    }

    public void setData(login_model.data data) {
        this.data = data;
    }

    public String getUserId() {
        return status;
    }

    public void setUserId(String userId) {
        status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String mobileNo;
    private String Password;


    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public class data
    {
        public int userId ;
        public String authToken ;

        public data(int userId, String authToken) {
            this.userId = userId;
            this.authToken = authToken;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }
    }

}
