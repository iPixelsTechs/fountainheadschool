package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 01-02-2018.
 */

public class TUpdateDateTimeDetails {
    ArrayList<TransportAttandanceUpdateDateTimeDetails> TransportAttandanceUpdateDateTimeDetails;

    public ArrayList<kinfo.darshit.dsp.f_school.Model.TransportAttandanceUpdateDateTimeDetails> getTransportAttandanceUpdateDateTimeDetails() {
        return TransportAttandanceUpdateDateTimeDetails;
    }

    public void setTransportAttandanceUpdateDateTimeDetails(ArrayList<kinfo.darshit.dsp.f_school.Model.TransportAttandanceUpdateDateTimeDetails> transportAttandanceUpdateDateTimeDetails) {
        TransportAttandanceUpdateDateTimeDetails = transportAttandanceUpdateDateTimeDetails;
    }
}
