package kinfo.darshit.dsp.f_school.Model;

import java.util.ArrayList;

/**
 * Created by COMP on 07-07-2017.
 */

public class GetRoute {

    private boolean status;

    public int routeId;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int timeSlotId;

    public int getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(int timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public boolean getStatus() { return this.status; }

    public void setStatus(boolean status) { this.status = status; }

    private String message;

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    private ArrayList<Datum> data;

    public ArrayList<Datum> getData() { return this.data; }

    public void setData(ArrayList<Datum> data) { this.data = data; }
    public class Datum
    {
        private String routeName;

        public String getRouteName() {
            return routeName;
        }

        public void setRouteName(String routeName) {
            this.routeName = routeName;
        }

        private int routeId;

        public int getRouteId() { return this.routeId; }

        public void setRouteId(int routeId) { this.routeId = routeId; }

        private boolean isNightOutRoot;

        public boolean getIsNightOutRoot() { return this.isNightOutRoot; }

        public void setIsNightOutRoot(boolean isNightOutRoot) { this.isNightOutRoot = isNightOutRoot; }

        private boolean isPerminant;

        public boolean getIsPerminant() { return this.isPerminant; }

        public void setIsPerminant(boolean isPerminant) { this.isPerminant = isPerminant; }
    }
}
