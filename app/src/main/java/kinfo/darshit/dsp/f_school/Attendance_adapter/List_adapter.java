package kinfo.darshit.dsp.f_school.Attendance_adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import kinfo.darshit.dsp.f_school.Attendance_report;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Report_data;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceDetail;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceMaster;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import kinfo.darshit.dsp.f_school.R;
import kinfo.darshit.dsp.f_school.login;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by COMP on 01-06-2017.
 */

public class List_adapter
    extends BaseAdapter {

Activity attends;
        Context context;

    ArrayList<Report_data> atten=new ArrayList<>();
String token="",arrive_datetime;

        public List_adapter(Context context, ArrayList<Report_data> atten,String token,Activity attends) {
            this.context = context;
            this.atten=atten;
            this.token=token;
            this.attends=attends;
        }


        @Override
        public int getCount() {
            return atten.size();
        }

        @Override
        public Object getItem(int position) {
            return atten.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder = null;


            ;


            if (convertView == null){

                LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

                viewHolder = new ViewHolder();

                convertView = layoutInflater.inflate(R.layout.adapter_list,null);


                viewHolder.img= (AppCompatImageView)convertView.findViewById(R.id.send_flag);
                viewHolder.route= (TextView)convertView.findViewById(R.id.route);
                viewHolder.date= (TextView)convertView.findViewById(R.id.date);
                viewHolder.sn_date=(TextView)convertView.findViewById(R.id.sn_date);
                viewHolder.card_view1=(CardView)convertView.findViewById(R.id.card_view1);


                convertView.setTag(viewHolder);


            }else {

                viewHolder = (ViewHolder)convertView.getTag();

            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            /*

            viewHolder.title.setTag(beans.getId());
            viewHolder.rupee.setText(beans.getRupee());
            viewHolder.ratings.setRating(Float.parseFloat(beans.getRatings()));
  */
            viewHolder.route.setText(atten.get(position).getRoute());
            viewHolder.date.setText(atten.get(position).getDate());
            viewHolder.sn_date.setText(atten.get(position).getSend_date());
            viewHolder.img.setTag(atten.get(position).getId());
            if(position==0)
            viewHolder.card_view1.setVisibility(View.VISIBLE);
else
                viewHolder.card_view1.setVisibility(View.GONE);
            Log.e("data_img",atten.get(position).getIsSend().toString());
            if (atten.get(position).getIsSend().toString().equals("1")) {
                viewHolder.img.setImageResource(R.drawable.ic_check_mark);
            }
            else
            {  viewHolder.img.setImageResource(R.drawable.ic_synchronization_arrows);}

viewHolder.img.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Log.e("Master_id",view.getTag().toString());
        //Toast.makeText(context,"hiii/"+view.getTag().toString(),Toast.LENGTH_LONG).show();
        for(int i=0;i<atten.size();i++)
        {
            if(view.getTag().toString().equals(atten.get(i).getId()+""))
            {
                if(!atten.get(i).getIsSend().toString().equals("1"))
                {
                   // send_attendance(Integer.parseInt(view.getTag().toString()));
                }
                else
                {
                    Toast.makeText(attends, "Already Updated", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
});
            viewHolder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("Master_id",view.getTag().toString());

                    send_attendance(Integer.parseInt(view.getTag().toString()));
                }
            });
           /* try {
                if (atten.get(position).getSend().equals(true)) {
                    viewHolder.img.setImageResource(R.drawable.ic_check_mark);
                }
              else
                {  viewHolder.img.setImageResource(R.drawable.ic_synchronization_arrows);}
            }catch(Exception e)
            {
                Log.e("img_error",e.getMessage());
            }*/
            return convertView;
        }

        private class ViewHolder{

            ImageView image,veg,non_veg;
            TextView route;
            TextView date,sn_date;
            AppCompatImageView img;
            CardView card_view1;


        }
    public void send_attendance(final int id)
    {

        final ProgressDialog mProgressDialog=new ProgressDialog(attends);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();



        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl("http://transport.m-3technologies.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice=retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy, HH:mm aaa");
        final String date = df.format(Calendar.getInstance().getTime());

        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization",token);

        JSONObject Main_json = new JSONObject();
        JSONArray TransportAttandanceMaster = new JSONArray();

        JSONObject master = new JSONObject();

        Database db=new Database(context,"table",null,1);
        db.Opendb();

        //    Cursor cr=db.get_Student_attendance_data(id);

        ArrayList<TransportAttandanceMaster> tm=new ArrayList<TransportAttandanceMaster>();


        Cursor cr=db.get_masterid(id);
        Cursor ch;
        try {
            if (cr.moveToFirst()) {
                do {
                    int route=Integer.parseInt(cr.getString(cr.getColumnIndex("appRouteMasterId")));
                    String attend_date=cr.getString(cr.getColumnIndex("appSendDateTime"));
                    Log.e("route",route+"");
                    ch=db.get_Student_attendance_data(id);
                    if(ch.moveToFirst())
                    {
                        JSONArray TransportAttandanceDetail = new JSONArray();
                        ArrayList<TransportAttandanceDetail>td=new ArrayList<TransportAttandanceDetail>();
                        do{
                            JSONObject detail = new JSONObject();
                            //appStudentId Integer,appStaffId Integer,appStopId Integer,isAttend Boolean,appCreatedDateTime varchar)
                            int stud_id=Integer.parseInt(ch.getString(ch.getColumnIndex("appStudentId")));
                            int staff_id=Integer.parseInt(ch.getString(ch.getColumnIndex("appStaffId")));
                            int stop_id=Integer.parseInt(ch.getString(ch.getColumnIndex("appStopId")));
                            Boolean isAttend=Boolean.parseBoolean(ch.getString(ch.getColumnIndex("isAttend")));
                            String r_date=cr.getString(cr.getColumnIndex("appCreatedDateTime"));
                            arrive_datetime=cr.getString(cr.getColumnIndex("routeDestinationTime"));
                            detail.put("studentId", stud_id);
                            detail.put("staffId", staff_id);
                            detail.put("stopId", stop_id);
                            detail.put("isAttend", isAttend);
                            detail.put("r_date", r_date);
                            TransportAttandanceDetail.put(detail);
                            Log.e("child_data",stud_id+"---"+staff_id+"---"+stop_id+"----"+isAttend+"----"+r_date);
                            TransportAttandanceDetail tadetail=new TransportAttandanceDetail(stud_id,staff_id,stop_id,isAttend,r_date);
                            td.add(tadetail);

                        }while (ch.moveToNext());
                        master.put("transportAttandanceDetail", TransportAttandanceDetail);
                        TransportAttandanceMaster clmaster=new TransportAttandanceMaster(route,attend_date,arrive_datetime,td);
                        tm.add(clmaster);
                    }
                } while (cr.moveToNext());
                TransportAttandanceMaster.put(master);
                Main_json.put("transportAttandanceMaster", TransportAttandanceMaster);

            }
        } catch (Exception e) {
            Log.d("Error tasklist", e.getMessage().toString());
        } finally {

        }
               send_attendance st=new send_attendance(tm);

        Log.e("sendata", Main_json.toString());

        Call<send_attendance> call = loginservice.send_data(map, st);

        call.enqueue(new Callback<send_attendance>() {
            @Override
            public void onResponse(Call<send_attendance> call, Response<send_attendance> response) {

                Log.e("result-code", response.message());
                Log.e("result-code", response.body().getMessage());
                if (response.body().getMessage().equals("success")) {
                    Database db=new Database(context,"table",null,1);
                    db.Opendb();
                    db.update_task(id,date);
                    for(int i=0;i<atten.size();i++)
                    {
                        if(atten.get(i).getId()==id)
                        {
                            atten.get(i).setIsSend("1");
                            notifyDataSetChanged();
                        }
                    }
                } else {

                }

                mProgressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<send_attendance> call, Throwable t) {
                Log.e("error", t.getMessage());

                mProgressDialog.dismiss();

            }
        });

        notifyDataSetChanged();

    }

    }


