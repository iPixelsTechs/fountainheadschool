package kinfo.darshit.dsp.f_school.Attendance_adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.vipulasri.timelineview.TimelineView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import kinfo.darshit.dsp.f_school.Absent_list;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Attendance_Result;
import kinfo.darshit.dsp.f_school.Model.Student_list;
import kinfo.darshit.dsp.f_school.Model.TUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceDetail;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceMaster;
import kinfo.darshit.dsp.f_school.Model.TransportAttandanceUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import kinfo.darshit.dsp.f_school.R;
import kinfo.darshit.dsp.f_school.Students_2;
import kinfo.darshit.dsp.f_school.attendance;
import kinfo.darshit.dsp.f_school.login;
import kinfo.darshit.dsp.f_school.spalsh;
import kinfo.darshit.dsp.f_school.utils.VectorDrawableUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static kinfo.darshit.dsp.f_school.common.url;

/**
 * Created by HP-HP on 05-12-2015.
 */
public class Real_Adapter extends BaseAdapter {

    Context context;
    ArrayList<attendance> bean;
    ArrayList<Students> bean_absent=new ArrayList<Students>(300);
    ArrayList<Students> present = new ArrayList<>();
    ArrayList<Students> absent = new ArrayList<>();
    Boolean istofs=false;
    Typeface fonts1, fonts2;
    //RecyclerView absent;
    int tot_absent,tot_present;
    TextView tot_ab,tot_pre;
    ArrayList<Students> items=new ArrayList<>();
    ArrayList<attendance> attend = new ArrayList<>();
    ArrayList<TransportAttandanceMaster> tm=new ArrayList<>();
    Attendance_Result Aresult=new Attendance_Result();
    String  send_date,token;
    Boolean send_flag = false;
    int id,std_routeid;
    Database db;
    String arrive_date;
    Boolean flag=false;
    int attendance_id=0;
    ArrayList<TransportAttandanceUpdateDateTimeDetails> details=new ArrayList<>();
    String url;

    public Real_Adapter(Context context, ArrayList<attendance> bean,Boolean istofs,int std_routeid,String token, int attendance_id,String url) {

        this.istofs=istofs;
        this.context = context;
        this.bean = bean;
        this.std_routeid=std_routeid;
        this.token=token;
        this.attendance_id=attendance_id;
        this.url=url;

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //@Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        Real_Adapter.ViewHolder viewHolder = null;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.main_adapter, null);
        viewHolder = new Real_Adapter.ViewHolder();
        viewHolder.area = (TextView) convertView.findViewById(R.id.area);
        //no_of_stud
        viewHolder.no_of_stud = (TextView) convertView.findViewById(R.id.no_of_stud);
        viewHolder.time = (TextView) convertView.findViewById(R.id.time);
        viewHolder.attend = (RecyclerView) convertView.findViewById(R.id.grid);
        viewHolder.absent = (RecyclerView) convertView.findViewById(R.id.absent);
        viewHolder.imgbutn=(ImageButton)convertView.findViewById(R.id.img_btn);
        viewHolder.status12 = (ImageView) convertView.findViewById(R.id.status12);
        viewHolder.mTimelineView = (TimelineView) convertView.findViewById(R.id.time_marker);
        viewHolder.ln_background=(LinearLayout)convertView.findViewById(R.id.ln_back_color);
        viewHolder.status12.setTag(position);
        viewHolder.imgbutn.setTag(position);
        //       viewHolder.status12.setClickable(true);
        viewHolder.attend.setHasFixedSize(true);

        final Real_Adapter.ViewHolder holder = viewHolder;



        try {
            if(bean.get(position).getFlag()=="0")
            {
                viewHolder.imgbutn.setImageResource(R.drawable.ic_check_mark);
                viewHolder.ln_background.setBackgroundColor(context.getResources().getColor(R.color.current));
            }
            else if(bean.get(position).getFlag()=="1")
            {
                viewHolder.imgbutn.setImageResource(R.drawable.ic_pending_mark);
                viewHolder.ln_background.setBackgroundColor(context.getResources().getColor(R.color.passed));
            }
            else if(bean.get(position).getFlag()=="2")
            {
                viewHolder.imgbutn.setImageResource(R.drawable.ic_pending_mark);
                viewHolder.ln_background.setBackgroundColor(context.getResources().getColor(R.color.pending));
            }

            Log.e("position", position + "");
            //viewHolder.area.setText(bean.get(position).getArea().toString());
            viewHolder.area.setText(bean.get(position).getArea().toString());
            viewHolder.time.setText(bean.get(position).getTime().toString());
            final GridViewAdapter gridViewAdapter;
            GridLayoutManager layoutManager = new GridLayoutManager(context, 5);
            final GridLayoutManager absent_stud = new GridLayoutManager(context, 5);
            viewHolder.attend.setLayoutManager(layoutManager);
            gridViewAdapter = new GridViewAdapter(context, bean.get(position).getPresent_stud(),position);
            viewHolder.attend.setAdapter(gridViewAdapter);

            viewHolder.absent.setLayoutManager(absent_stud);
            absent_adapter abgridViewAdapter;
            abgridViewAdapter = new absent_adapter(context, bean.get(position).getAb_stud(),position);
            holder.absent.setAdapter(abgridViewAdapter);
            viewHolder.no_of_stud.setText("PAX: "+bean.get(position).getPresent_stud().size());



Log.e("status:istofs",istofs+"");


            gridViewAdapter.setOnItemClickListener(new GridViewAdapter.ClickListener() {
                @Override
                public void onItemClick(int pos, View v) {

                    Log.e("Main_postions",position+"");
                        int pre_color;
                  //  try {
                        LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
                        int arr_position=Integer.parseInt(ln_stud.getTag().toString());
                        TextView name=(TextView)v.findViewById(R.id.name);
                        Log.e("views",arr_position+"");
                    ArrayList<Students> pas = new ArrayList<Students>();
                    ArrayList<Students> abs = new ArrayList<Students>();
                        if(bean.get(arr_position).getFlag()=="0") {
                            Students ab = new Students();
                            ImageView img = (ImageView) v.findViewById(R.id.img);
                            if (img.getTag().toString().equals("false")) {
                                ln_stud.setBackgroundColor(context.getResources().getColor(R.color.present));
                                img.setTag("true");
                                pre_color=context.getResources().getColor(R.color.present);

                            } else {
                                ln_stud.setBackgroundColor(context.getResources().getColor(R.color.absent));
                                img.setTag("false");
                                pre_color=context.getResources().getColor(R.color.absent);
                            }


                            String flag = img.getTag().toString();

                            pas = bean.get(arr_position).getPresent_stud();
                            abs = bean.get(arr_position).getAb_stud();
                            int id = Integer.parseInt(name.getTag().toString());
                            Boolean status=true;
                            if(!istofs) {
                                for (int i = 0; i < abs.size(); i++) {
                                    if (abs.get(i).getId() == id) {//public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {
                                        Toast.makeText(context, "Student is Absent", Toast.LENGTH_LONG).show();
                                        ln_stud.setBackgroundColor(context.getResources().getColor(R.color.absent));
                                        img.setTag("false");
                                        flag="false";
                                        status=false;
                                    }
                                }
                                if(status) {
                                    for (int k = 0; k < pas.size(); k++) {
                                        Log.e("checkedid", pas.get(k).getId() + "");

                                        if (pas.get(k).getId() == id) {//public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {
                                            ab = new Students(pas.get(k).getId(), pas.get(k).getName(), pas.get(k).getImage(), pas.get(k).getCategory(), true, pas.get(k).getEmergencyContactNo(), pas.get(k).getFskId(), pas.get(k).getHrisEmployeeID(), pas.get(k).getGrade(), pas.get(k).getSection(),pas.get(k).getIssearched(),pas.get(k).getStopName(),pre_color,pas.get(k).getConcentflag(),pas.get(k).getStopid(),pas.get(k).getAbsent());
                                            pas.get(k).setPre_color(pre_color);
                                        }

                                    }
                                }


                            }
                            else {
                                for (int k = 0; k < pas.size(); k++) {
                                    Log.e("checkedid", pas.get(k).getId() + "");
                                    if (pas.get(k).getId() == id) {//public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {
                                        ab = new Students(pas.get(k).getId(), pas.get(k).getName(), pas.get(k).getImage(), pas.get(k).getCategory(), true, pas.get(k).getEmergencyContactNo(), pas.get(k).getFskId(), pas.get(k).getHrisEmployeeID(), pas.get(k).getGrade(), pas.get(k).getSection(),pas.get(k).getIssearched(),pas.get(k).getStopName(),pas.get(k).getPre_color(),pas.get(k).getConcentflag(),pas.get(k).getStopid(),pas.get(k).getAbsent());
                                        pas.get(k).setPre_color(pre_color);
                                    }

                                }
                            }
                            Boolean a = true;


                            if (flag.equals("true")) {
                                present.add(ab);
                            } else {
                                //   Students ab=new Students(holder.area.getText().toString(),name.getText().toString(),name.getText().toString(),context.getResources().getColor(R.color.white),context.getResources().getColor(R.color.white),true);
                               /* for (Students atten : present) {
                                    if (atten.getId() == ab.getId()) {
                                        Log.e("Matched", ab.getId() + "");
                                        present.remove(atten);
                                    }

                                }*/
                                for(int i=0;i<present.size();i++)
                                {
                                    if(present.get(i).getId()==ab.getId())
                                    {
                                        present.remove(i);
                                    }
                                }
                            }

                            if (present.size() > 0) {
                                for (int i = 0; i < present.size(); i++) {
                                    Log.e("abStudent", present.get(i).getId() + "");
                                }
                            }


                        }
                        else
                        {
                            Toast.makeText(context,"Please follow the route",Toast.LENGTH_LONG).show();
                        }




                }

                @Override
                public void onItemLongClick(int position, View v) {
                    try {
                        LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
                        int arr_position=Integer.parseInt(ln_stud.getTag().toString());
                        TextView name=(TextView)v.findViewById(R.id.name);
                  //      Toast.makeText(context, name.getText().toString(), Toast.LENGTH_LONG).show();
                        ArrayList<Students> pas = new ArrayList<Students>();
                        pas = bean.get(arr_position).getPresent_stud();
                        int id = Integer.parseInt(name.getTag().toString());
                        Students ab=new Students();
                        String area_nm=bean.get(arr_position).getArea();
                        // public void dialog(String sname,String scat,String semg,String sgrade,String ssection,String simg) {
                        for (int k = 0; k < pas.size(); k++) {
                            Log.e("checkedid", pas.get(k).getId() + "");
                            if (pas.get(k).getId() == id) {//public Students(int id, String name, String image, String category) {
                                //ab = new Students(pas.get(k).getId(), pas.get(k).getName(), pas.get(k).getImage(), pas.get(k).getCategory(),true);
                               //ab = new Students(pas.get(k).getId(), pas.get(k).getName(), pas.get(k).getImage(), pas.get(k).getCategory(), true, pas.get(k).getEmergencyContactNo(), pas.get(k).getFskId(), pas.get(k).getHrisEmployeeID(), pas.get(k).getGrade(), pas.get(k).getSection(),pas.get(k).getIssearched(),pas.get(k).getStopName(),pas.get(k).getPre_color(),pas.get(k).getConcentflag(),pas.get(k).getStopid());
                                ab = new Students(pas.get(k).getId(), pas.get(k).getName(), pas.get(k).getImage(), pas.get(k).getCategory(),true,pas.get(k).getEmergencyContactNo(),pas.get(k).getFskId(),pas.get(k).getHrisEmployeeID(),pas.get(k).getGrade(),pas.get(k).getSection(),pas.get(k).getIssearched(),pas.get(k).getStopName(),pas.get(k).getPre_color(),pas.get(k).getConcentflag(),pas.get(k).getStopid(),pas.get(k).getAbsent());
                                break;
                            }

                        }
                        dialog(ab.getName(),ab.getCategory(),ab.getEmergencyContactNo(),ab.getGrade(),ab.getSection(),ab.getImage(),ab.getStopName(),ab.getIssearched(),ab.getId(),arr_position);
                    }
                    catch(Exception e) {
                        Log.e("Error", e.getMessage());
                    }
                }
            });


            abgridViewAdapter.setOnItemClickListener(new absent_adapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {
int pre_color;
                    if(istofs) {
                        LinearLayout ln_stud = (LinearLayout) v.findViewById(R.id.ln_stud);
                        int arr_postion = Integer.parseInt(ln_stud.getTag().toString());
                        TextView name = (TextView) v.findViewById(R.id.name);
                        ImageView img = (ImageView) v.findViewById(R.id.img);
                        String pos = name.getTag().toString();

                        ArrayList<Students> pas = new ArrayList<Students>();
                        ArrayList<Students> abs = new ArrayList<Students>();

                        pas = bean.get(arr_postion).getPresent_stud();
                        abs = bean.get(arr_postion).getAb_stud();

                        Log.e("total_abs", abs.size()+ "");


                        int id = Integer.parseInt(name.getTag().toString());


                            if (img.getTag().toString().equals("false")) {
                                ln_stud.setBackgroundColor(context.getResources().getColor(R.color.present));
                                img.setTag("true");
                                pre_color=context.getResources().getColor(R.color.present);
                                for (int k = 0; k < abs.size(); k++) {
                                   // Log.e("id", id + "");
                                    if (abs.get(k).getId() == id) {//public Students(int id, String name, String image, String category) {
                                            Students ab = new Students();
                                            Log.e("id", abs.get(k).getId() + "");
                                            Log.e("Name", abs.get(k).getName() + "");
                                            Log.e("image", abs.get(k).getImage() + "");
                                            Log.e("cate", abs.get(k).getCategory() + "");

                                        int sid = abs.get(k).getId();
                                        String sname = abs.get(k).getName();
                                        String category = abs.get(k).getCategory();
                                        String Image = abs.get(k).getImage();
                                        String eno=abs.get(k).getEmergencyContactNo();
                                        String fskid=abs.get(k).getFskId();
                                        String eid=abs.get(k).getEmergencyContactNo();
                                        String grade=abs.get(k).getGrade();
                                        String section=abs.get(k).getSection();
                                        boolean getIssearched=abs.get(k).getIssearched();
                                        String StopName=abs.get(k).getStopName();
                                        String concentflag=abs.get(k).getConcentflag();
                                        boolean isabsent=abs.get(k).getAbsent();
                                        int stopid=abs.get(k).getStopid();



                                            ab = new Students(sid, sname, Image, category, true, eno, fskid, eid, grade, section,getIssearched,StopName,pre_color,concentflag,stopid,isabsent);
                                            present.add(ab);
                                        abs.get(k).setPre_color(pre_color);


                                    }
                                }
                            } else {
                                ln_stud.setBackgroundColor(context.getResources().getColor(R.color.absent));
                                img.setTag("false");
                                pre_color=context.getResources().getColor(R.color.absent);
                                for (int i = 0; i < present.size(); i++) {
                                    if (present.get(i).getId() == id) {
                                        present.remove(i);
                                    }
                                }

                                for (int k = 0; k < abs.size(); k++) {
                                    // Log.e("id", id + "");
                                    if (abs.get(k).getId() == id) {//public Students(int id, String name, String image, String category) {
                                        abs.get(k).setPre_color(pre_color);
                                    }
                                }
                            }




                      //  Log.e("Selected_id", id + "");

                    }
                }

                @Override
                public void onItemLongClick(int position, View v) {

                }
            });

            viewHolder.imgbutn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        Students ab=new Students();
                    int pre_color=context.getResources().getColor(R.color.absent);

                        Log.e("length of absent", absent.size() + "");
                        Log.e("length of present", present.size() + "");
                        Log.e("Length of stud", bean.get(position).getPresent_stud().size() + "");
                        Log.e("current possition", view.getTag().toString());
                        if (bean.get(position).getFlag().equals("0")) {
                              ArrayList<Students> student = new ArrayList<>();
                              ArrayList<Students> abdelete = new ArrayList<>();
                              Boolean isattend =false;

                             /* for(int i=0;i<bean_absent.size();i++)
                              {
                                  Log.e("pre_stud_id",bean_absent.get(i).getId()+"");
                                  Log.e("pre_stud_name",bean_absent.get(i).getName());
                              }
                              for(int i=0;i<absent.size();i++)
                              {
                                  Log.e("ab_stud_id",absent.get(i).getId()+"");
                                  Log.e("ab_stud_name",absent.get(i).getName());
                              }*/
                              if(istofs) {
                                for (int j = 0; j < present.size(); j++) {
                                    for (int i = 0; i < absent.size(); i++) {
                                        Log.e("preStudent_id", present.get(j).getId() + "");
                                        Log.e("abStudent_id", absent.get(i).getId() + "");
                                        if (absent.get(i).getId() == present.get(j).getId()) {
                                            Log.e("Student_id", absent.get(i).getId() + "");
                                            Log.e("Student_Name", absent.get(i).getName() + "");
                                            Log.e("Student_Image", absent.get(i).getImage() + "");
                                            Log.e("Student_Category", absent.get(i).getCategory() + "");
                                            present.get(j).setPre_color(pre_color);
                                            absent.remove(i);
                                            student.add(present.get(j));

                                            for (int k = 0; k < Integer.parseInt(view.getTag().toString()); k++) {

                                                for (int l = 0; l < bean.get(k).getPresent_stud().size(); l++) {
                                                    if (bean.get(k).getPresent_stud().get(l).getId() == present.get(j).getId() && bean.get(k).getPresent_stud().get(l).getCategory() == present.get(j).getCategory()) {
                                                        Log.e("Student_id", bean.get(k).getPresent_stud().get(l).getId() + "");
                                                        Log.e("Student_Name", bean.get(k).getPresent_stud().get(l).getName() + "");
                                                        Log.e("Student_Image", bean.get(k).getPresent_stud().get(l).getImage() + "");
                                                        Log.e("Student_Category", bean.get(k).getPresent_stud().get(l).getCategory() + "");
                                                        bean.get(k).getPresent_stud().remove(l);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                             /* if (Integer.parseInt(view.getTag().toString()) != bean.size()) {*/
                            for (int i = 0; i < bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().size(); i++) {
                                int stud_id = bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getId();
                                String cate=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getCategory();
                                Log.e("Student_id",stud_id+"");
                                Boolean matched = true;
                                for (int j = 0; j < present.size(); j++) {
                                    int pre_stud_id = present.get(j).getId();
                                    String cat=present.get(j).getCategory();
                         //           Log.e("Presentid",pre_stud_id+"");

                                    if (pre_stud_id==stud_id && cate.equals(cat)) {
                                        matched = false;
                                        isattend=present.get(j).getStatus();
                                    }
                                    else
                                    {
                                        Log.e("------","-------------------------------");
                                        Log.e("student_id",present.get(j).getId()+"");
                                        Log.e("student",present.get(j).getName()+"");
                                        Log.e("student_attendance",present.get(j).getStatus()+"");
                                        Log.e("------","-------------------------------");
                                    }
                                }
                                //public Students(String emergencyContactNo, String fskId, String hrisEmployeeID, String grade, String section) {
                                int id = bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getId();
                                String name = bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getName();
                                String category = bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getCategory();
                                String Image = bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getImage();
                                String eno=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getEmergencyContactNo();
                                String fskid=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getFskId();
                                String eid=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getHrisEmployeeID();
                                String grade=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getGrade();
                                String section=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getSection();
                                boolean getIssearched=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getIssearched();
                                String StopName=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getStopName();
                                String concentflag=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getConcentflag();
                                int stopid=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getStopid();
                                boolean isabsent=bean.get(Integer.parseInt(view.getTag().toString())).getPresent_stud().get(i).getAbsent();
                                if (matched) {
                                    ab=new Students(id,name,Image,category,false,eno,fskid,eid,grade,section,getIssearched,StopName,pre_color,concentflag,stopid,isabsent);

                                    if(!istofs)
                                    {
                                        for (int k = 0; k < absent.size(); k++) {
                                            if (absent.get(k).getId() == id) {

                                                absent.remove(k);
                                            }
                                        }
                                    }
                                    absent.add(ab);
                                    student.add(ab);
                                }
                                else {

                                    /*for (int k = 0; k < absent.size(); k++) {
                                        if (absent.get(k).getId() == id) {
                                            absent.remove(k);
                                        }
                                    }*/

                                    ab=new Students(id,name,Image,category,true,eno,fskid,eid,grade,section,getIssearched,StopName,pre_color,concentflag,stopid,isabsent);
                                    student.add(ab);

                                }

                            }
                                  DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
                                  String date = df.format(Calendar.getInstance().getTime());
                        attendance model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())).getAreid(),bean.get(Integer.parseInt(view.getTag().toString())).getArea(),bean.get(Integer.parseInt(view.getTag().toString())).getTime(), student, abdelete,"0",date);

                        bean.set(Integer.parseInt(view.getTag().toString()), model);
                            notifyDataSetChanged();
                            if(!istofs){
                                if(position==0) {
                                    send_attendance(model);
                                }
                                //if client says call api on every stage then uncomment this code
                               /* else
                                {
                                    send_update(model);
                                }*/
                            }
                        //Add Absent Student in Next Area
                           // Log.e("size",Integer.parseInt(view.getTag().toString())+"arr"+bean.size());
                        if(Integer.parseInt(view.getTag().toString())+1==bean.size())
                        {
                            Log.e("position",bean.get(position)+"-true");
                            student = bean.get(Integer.parseInt(view.getTag().toString()) ).getPresent_stud();
                            model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())).getAreid(),bean.get(Integer.parseInt(view.getTag().toString()) ).getArea(), bean.get(Integer.parseInt(view.getTag().toString())).getTime(), student, absent, "0",date);
                            bean.set(Integer.parseInt(view.getTag().toString()), model);
                        }
                        else {
                            Log.e("position",bean.get(position)+"-false");
                            student = bean.get(Integer.parseInt(view.getTag().toString()) + 1).getPresent_stud();
                            model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())+1).getAreid(),bean.get(Integer.parseInt(view.getTag().toString()) + 1).getArea(), bean.get(Integer.parseInt(view.getTag().toString()) + 1).getTime(), student, absent, "0",date);
                            bean.set(Integer.parseInt(view.getTag().toString()) + 1, model);
                        }


                        bean.get(position).setFlag("1");
                        if(position+1==bean.size())
                        {

                        }
                        else
                        {
                            bean.get(position+1).setFlag("0");
                        }
                            notifyDataSetChanged();
                   //     }
                }
                else
               {

                    Toast.makeText(context, "Please follow the route", Toast.LENGTH_SHORT).show();
                }
                }
            });

        }catch(Exception e )
        {
            Log.e("Error msg",e.getMessage());

        }
        return convertView;
    }


    private class ViewHolder{

        private TextView area,time,no_of_stud;
        private RecyclerView attend,absent;
        private ImageView status12;
        private TimelineView mTimelineView;
        private ImageButton imgbutn;
        private LinearLayout ln_background;

    }

    public void dialog(String sname, String scat, String semg, String sgrade, String ssection, String simg, String area_nm, boolean isstud_search, final int studid, final int std_position) {
        TextView Name,category,emg,grade,section,area;
        ImageView img;

        View view;
        Button remove;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        //view = layoutInflater.inflate(R.layout.main_adapter, null);

        View v = layoutInflater.inflate(R.layout.stud_details, null);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setView(v);
        Name = (TextView) v.findViewById(R.id.txt_nm);
        category = (TextView) v.findViewById(R.id.txt_cat);
        emg = (TextView) v.findViewById(R.id.txt_emcal);
        grade=(TextView)  v.findViewById(R.id.txt_grade);
        section=(TextView)  v.findViewById(R.id.txt_section);
        area=(TextView)  v.findViewById(R.id.txt_bus_stop);
        img=(ImageView)v.findViewById(R.id.profile_pic);
        remove=(Button)v.findViewById(R.id.btn_remove);


        if(isstud_search)
            remove.setVisibility(View.VISIBLE);
        else
            remove.setVisibility(View.GONE);



        Name.setText(sname);
        category.setText(scat);
        emg.setText(semg);
        grade.setText(sgrade);
        section.setText(ssection);
        area.setText(area_nm);
        Glide.with(context)
                .load(simg)
                .placeholder(R.drawable.fountain)
                .error(R.drawable.fountain)
                .into(img);
        final AlertDialog ad = builder.show();

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i=0;i<bean.get(std_position).getPresent_stud().size();i++)
                {
                    for(int j=0;j<bean.get(std_position).getPresent_stud().size();j++)
                    {
                        if(bean.get(std_position).getPresent_stud().get(j).getId()==studid)
                        {
                            bean.get(std_position).getPresent_stud().remove(j);
                            notifyDataSetChanged();
                            ad.dismiss();
                        }
                    }
                }
            }
        });

    }


    public void send_attendance(attendance attend) {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Log.e("Current Url",url);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);

        JSONObject Main_json = new JSONObject();
        JSONArray TransportAttandanceMaster = new JSONArray();

        JSONObject master = new JSONObject();
        try {
            ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();
                    Log.e("areaid", attend.getAreid() + "");
                    master.put("routeId", std_routeid);
                    master.put("atttendaceDateTime", send_date);
                    JSONArray TransportAttandanceDetail = new JSONArray();
                    //ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();

                    for (int j = 0; j < attend.getPresent_stud().size(); j++) {

                        JSONObject detail = new JSONObject();
                        int Stud_id = 0, staff = 0;
                        Boolean isAttend = false;
                        int stopid = attend.getPresent_stud().get(j).getStopid();

                        if (attend.getPresent_stud().get(j).getCategory().equals("Student")) {
                            detail.put("studentId", attend.getPresent_stud().get(j).getId());
                            detail.put("staffId", 0);
                            Stud_id = attend.getPresent_stud().get(j).getId();
                            staff = 0;

                        } else {
                            detail.put("studentId", 0);
                            detail.put("staffId", attend.getPresent_stud().get(j).getId());
                            staff = attend.getPresent_stud().get(j).getId();
                            Stud_id = 0;
                        }
                        detail.put("stopId", attend.getPresent_stud().get(j).getStopid());
                        detail.put("isAttend", attend.getPresent_stud().get(j).getStatus());
                        TransportAttandanceDetail cldetails = new TransportAttandanceDetail(Stud_id, staff, stopid, attend.getPresent_stud().get(j).getStatus(), attend.getR_time());
                        TransportAttandanceDetail.put(detail);
                        td.add(cldetails);

                        Log.e("child-data", Stud_id + "---" + staff + "---" + stopid + "---" + attend.getPresent_stud().get(j).getStatus() + "---" + attend.getR_time() + "-----");

                    }

                    master.put("transportAttandanceDetail", TransportAttandanceDetail);

                    TransportAttandanceMaster clmaster = new TransportAttandanceMaster(std_routeid, send_date,arrive_date, td);
                    tm.add(clmaster);


            TransportAttandanceMaster.put(master);
            Main_json.put("transportAttandanceMaster", TransportAttandanceMaster);


        } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
        }
        try {
            send_attendance st = new send_attendance(tm);

            Log.e("sendata", st.toString());
            Call<Attendance_Result> call = loginservice.send_data_istofs(map, st);

            Log.e("s_length",attend+"");
            call.enqueue(new Callback<Attendance_Result>() {
                @Override
                public void onResponse(Call<Attendance_Result> call, Response<Attendance_Result> response) {

                    Log.e("result-code", response.message());
                    Log.e("result-code", response.body().getMessage());
                    if (response.body().getMessage().equals("success")) {

                        Attendance_Result result=response.body();
                        for(int i=0;i<result.getData().size();i++) {
                            attendance_id = result.getData().get(i).getAttendanceId();
                            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                            SharedPreferences.Editor e = sp.edit();
                            e.putInt("attendance_id", attendance_id);
                            e.commit();


                        }
                        notifyDataSetChanged();

                        send_flag = true;
                        Database db = new Database(context, "table", null, 1);
                        db.Opendb();
                        db.update_task(id, send_date);
                        Log.e("Send_status", send_flag + "");
                        db.close();




                    } else {
                        send_flag = false;
                    }

                    mProgressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<Attendance_Result> call, Throwable t) {
                    Log.e("error", t.getMessage());

                    mProgressDialog.dismiss();

                }
            });
        } catch (Exception e) {
            Log.e("error------",e.getMessage().toLowerCase());
        }
        //------------------------------------------------------------------

    }
    public void send_update(attendance attend) {
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setTitle("Sending Data...");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
        send_date = df.format(Calendar.getInstance().getTime());
        Log.e("date",send_date);
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization", token);

       JSONObject master = new JSONObject();
        try {
            ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();
            Log.e("areaid", attend.getAreid() + "");
            master.put("routeId", std_routeid);
            master.put("atttendaceDateTime", send_date);
            JSONArray TransportAttandanceDetail = new JSONArray();
            //ArrayList<TransportAttandanceDetail> td = new ArrayList<TransportAttandanceDetail>();

            for (int j = 0; j < attend.getPresent_stud().size(); j++) {
             int stud_id=0,staff_id=0;
                //(int attendanceId, int studentId, int staffId, int stopId, String atttendaceDateTime) {
                if (attend.getPresent_stud().get(j).getCategory().equals("Student")) {
                    stud_id= attend.getPresent_stud().get(j).getId();
                } else {
                    staff_id = attend.getPresent_stud().get(j).getId();
                }
                details.add(new TransportAttandanceUpdateDateTimeDetails(attendance_id,stud_id,staff_id,attend.getAreid(),send_date));
            }
            TUpdateDateTimeDetails tdt=new TUpdateDateTimeDetails();
            tdt.setTransportAttandanceUpdateDateTimeDetails(details);

            Call<Attendance_Result> call = loginservice.send_data_istofs2(map, tdt);

            Log.e("s_length",attend+"");
            call.enqueue(new Callback<Attendance_Result>() {
                @Override
                public void onResponse(Call<Attendance_Result> call, Response<Attendance_Result> response) {
                    if(response.isSuccessful())
                    {
                        Log.e("status","Data passed");
                    }
                    else
                    {
                        Log.e("status",response.code()+"");
                    }
                    mProgressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Attendance_Result> call, Throwable t) {
                    mProgressDialog.dismiss();
                }
            });




            } catch (Exception e) {
            Log.e("Error_in_json", e.getMessage());
            mProgressDialog.dismiss();
        }

        //------------------------------------------------------------------

    }


}
