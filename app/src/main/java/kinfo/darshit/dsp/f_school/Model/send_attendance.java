package kinfo.darshit.dsp.f_school.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by COMP on 14-07-2017.
 */

public class send_attendance implements Serializable {
    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("message")
    @Expose
    String message;

    String s_date;
    public send_attendance(ArrayList<TransportAttandanceMaster> transportAttandanceMaster) {
        this.transportAttandanceMaster = transportAttandanceMaster;

    }

    public String getS_date() {
        return s_date;
    }

    public void setS_date(String s_date) {
        this.s_date = s_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private ArrayList<TransportAttandanceMaster> transportAttandanceMaster;

    public ArrayList<TransportAttandanceMaster> getTransportAttandanceMaster() { return this.transportAttandanceMaster; }

    public void setTransportAttandanceMaster(ArrayList<TransportAttandanceMaster> transportAttandanceMaster) { this.transportAttandanceMaster = transportAttandanceMaster; }
   /* public class TransportAttandanceDetail
    {
        private int studentId;

        public int getStudentId() { return this.studentId; }

        public void setStudentId(int studentId) { this.studentId = studentId; }

        private int staffId;

        public int getStaffId() { return this.staffId; }

        public void setStaffId(int staffId) { this.staffId = staffId; }

        private int stopId;

        public int getStopId() { return this.stopId; }

        public void setStopId(int stopId) { this.stopId = stopId; }

        private boolean isAttend;

        public boolean getIsAttend() { return this.isAttend; }

        public void setIsAttend(boolean isAttend) { this.isAttend = isAttend; }
    }

    public class TransportAttandanceMaster
    {

        public TransportAttandanceMaster() {
        }

        private int routeId;

        public int getRouteId() { return this.routeId; }

        public void setRouteId(int routeId) { this.routeId = routeId; }

        private String atttendaceDateTime;

        public String getAtttendaceDateTime() { return this.atttendaceDateTime; }

        public void setAtttendaceDateTime(String atttendaceDateTime) { this.atttendaceDateTime = atttendaceDateTime; }

        private ArrayList<TransportAttandanceDetail> transportAttandanceDetail;

        public ArrayList<TransportAttandanceDetail> getTransportAttandanceDetail() { return this.transportAttandanceDetail; }

        public void setTransportAttandanceDetail(ArrayList<TransportAttandanceDetail> transportAttandanceDetail) { this.transportAttandanceDetail = transportAttandanceDetail; }
    }*/


}
