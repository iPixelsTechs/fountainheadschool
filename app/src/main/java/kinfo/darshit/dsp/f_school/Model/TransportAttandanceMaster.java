package kinfo.darshit.dsp.f_school.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by COMP on 14-07-2017.
 */

public class TransportAttandanceMaster implements Serializable {

    public TransportAttandanceMaster() {
    }

    public TransportAttandanceMaster(int routeId, String atttendaceDateTime,String routeDestinationTime, ArrayList<TransportAttandanceDetail> transportAttandanceDetail) {
        this.routeId = routeId;
        this.atttendaceDateTime = atttendaceDateTime;
        this.transportAttandanceDetail = transportAttandanceDetail;
        this.routeDestinationTime=routeDestinationTime;
    }

    private int routeId;

    public int getRouteId() { return this.routeId; }

    public void setRouteId(int routeId) { this.routeId = routeId; }
    private String routeDestinationTime;

    public String getRouteDestinationTime() {
        return routeDestinationTime;
    }

    public void setRouteDestinationTime(String routeDestinationTime) {
        this.routeDestinationTime = routeDestinationTime;
    }

    private String atttendaceDateTime;

    public String getAtttendaceDateTime() { return this.atttendaceDateTime; }

    public void setAtttendaceDateTime(String atttendaceDateTime) { this.atttendaceDateTime = atttendaceDateTime; }

    private ArrayList<TransportAttandanceDetail> transportAttandanceDetail;

    public ArrayList<TransportAttandanceDetail> getTransportAttandanceDetail() { return this.transportAttandanceDetail; }

    public void setTransportAttandanceDetail(ArrayList<TransportAttandanceDetail> transportAttandanceDetail) { this.transportAttandanceDetail = transportAttandanceDetail; }

}
