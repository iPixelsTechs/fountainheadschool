package kinfo.darshit.dsp.f_school.Attendance_adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.vipulasri.timelineview.TimelineView;

import java.util.ArrayList;

import kinfo.darshit.dsp.f_school.Model.Student_list;
import kinfo.darshit.dsp.f_school.R;
import kinfo.darshit.dsp.f_school.attendance;
import kinfo.darshit.dsp.f_school.utils.VectorDrawableUtils;





public class Main_adapter extends BaseAdapter {


    Context context;

    ArrayList<Student_list.Datum> bean;
    ArrayList<Students> bean_absent=new ArrayList<Students>(300);
    ArrayList<Students> present = new ArrayList<>();
    ArrayList<Students> absent = new ArrayList<>();
    Typeface fonts1, fonts2;


    public Main_adapter(Context context, ArrayList<Student_list.Datum> bean,ArrayList<Students> bean_absent) {


        this.context = context;
        this.bean = bean;
        this.bean_absent=bean_absent;
    }


    @Override
    public int getCount() {
        return bean.size();
    }

    @Override
    public Object getItem(int position) {
        return bean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //@Override
   public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = null;

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.main_adapter, null);
        viewHolder = new ViewHolder();
        viewHolder.area = (TextView) convertView.findViewById(R.id.area);
        viewHolder.time = (TextView) convertView.findViewById(R.id.time);
        viewHolder.attend = (RecyclerView) convertView.findViewById(R.id.grid);
        viewHolder.absent = (RecyclerView) convertView.findViewById(R.id.absent);
       viewHolder.imgbutn=(ImageButton)convertView.findViewById(R.id.img_btn);
        viewHolder.status12 = (ImageView) convertView.findViewById(R.id.status12);
        viewHolder.mTimelineView = (TimelineView) convertView.findViewById(R.id.time_marker);

        viewHolder.status12.setTag(position);
       viewHolder.imgbutn.setTag(position);
 //       viewHolder.status12.setClickable(true);
        viewHolder.attend.setHasFixedSize(true);
        final ViewHolder holder = viewHolder;

        try {
            Log.e("position", position + "");
            //viewHolder.area.setText(bean.get(position).getArea().toString());
            viewHolder.area.setText(bean.get(position).getStopName().toString());
            viewHolder.time.setText(bean.get(position).getStopTime().toString());
            final GridViewAdapter gridViewAdapter;
            GridLayoutManager layoutManager = new GridLayoutManager(context, 5);
            final GridLayoutManager absent_stud = new GridLayoutManager(context, 5);
            viewHolder.attend.setLayoutManager(layoutManager);
             //gridViewAdapter = new GridViewAdapter(context, bean.get(position).getPassengers(),position);
             //viewHolder.attend.setAdapter(gridViewAdapter);

            viewHolder.absent.setLayoutManager(absent_stud);
            absent_adapter abgridViewAdapter;
              abgridViewAdapter = new absent_adapter(context, absent,position);
              holder.absent.setAdapter(abgridViewAdapter);

/*
 if(position == 0) {
        viewHolder.mTimelineView.initLine(1);
                holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_inactive, ContextCompat.getColor(context,R.color.timeline)));
        //        holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker_inactive),ContextCompat.getColor(context,R.color.timeline));
    } else if (position==bean.size()-1){
        viewHolder.mTimelineView.initLine(2);
      //  holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_active, R.color.timeline));
        holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker_active),ContextCompat.getColor(context,R.color.timeline));
    }
    else {
        holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker), ContextCompat.getColor(context, R.color.timeline));
    }*/




/*
gridViewAdapter.setOnItemClickListener(new GridViewAdapter.ClickListener() {
        @Override
        public void onItemClick(int pos, View v) {

            Log.e("Main_postions",position+"");
            try {
                LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
                int arr_position=Integer.parseInt(ln_stud.getTag().toString());
                TextView name=(TextView)v.findViewById(R.id.name);

                Students ab=new Students();



                ImageView img=(ImageView)v.findViewById(R.id.img);

                if (img.getTag().toString().equals("false")) {
                    ln_stud.setBackgroundColor(context.getResources().getColor(R.color.absent));
                    img.setTag("true");

                } else {
                    ln_stud.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
                    img.setTag("false");
                }

                String flag=img.getTag().toString();
                int id=Integer.parseInt(name.getTag().toString());
                ArrayList<Student_list.Passenger> pas=new ArrayList<Student_list.Passenger>();
                pas=bean.get(arr_position).getPassengers();
                for(int k=0;k<pas.size();k++) {
                    if(pas.get(k).getId()==id)
                    {//public Students(int id, String name, String image, String category) {
                        ab=new Students(pas.get(k).getId(),pas.get(k).getName(),pas.get(k).getImage(),pas.get(k).getCategory());

                    }

                }
                Boolean a=true;
                if(flag.equals("true"))
                {
                    present.add(ab);
                }
                else
                {
                 //   Students ab=new Students(holder.area.getText().toString(),name.getText().toString(),name.getText().toString(),context.getResources().getColor(R.color.white),context.getResources().getColor(R.color.white),true);
                   for (Students atten:present)
                    {
                        if(atten.getId()==ab.getId())
                        {
                            Log.e("Matched",ab.getId()+"");
                            present.remove(atten);
                        }

                    }
                }

                if(present.size()>0) {
                    for (int i = 0; i < present.size(); i++) {
                        Log.e("abStudent", present.get(i).getId() + "");
                    }
                }



*//*
TextView name=(TextView)v.findViewById(R.id.name);
                Toast.makeText(context, name.getText().toString(), Toast.LENGTH_LONG).show();
*//*

            }
            catch(Exception e)
            { Log.e("Error",e.getMessage());
//                Log.e("Click position",bean.get(position).getStud().get(pos).getStudent().toString());
            }
        }

        @Override
        public void onItemLongClick(int position, View v) {
            try {

                TextView name=(TextView)v.findViewById(R.id.name);
                Toast.makeText(context, name.getText().toString(), Toast.LENGTH_LONG).show();
            }
            catch(Exception e) {
                Log.e("Error", e.getMessage());
            }
        }
    });*/


abgridViewAdapter.setOnItemClickListener(new absent_adapter.ClickListener() {
    @Override
    public void onItemClick(int position, View v) {
        LinearLayout ln_stud=(LinearLayout)v.findViewById(R.id.ln_stud);
        TextView name=(TextView)v.findViewById(R.id.name);
        ImageView img=(ImageView) v.findViewById(R.id.img);
        String pos=name.getTag().toString();
        Toast.makeText(context,name.getText().toString(),Toast.LENGTH_LONG).show();
        Students ab=new Students();
        ArrayList<Student_list.Passenger> pas=new ArrayList<Student_list.Passenger>();
        pas=bean.get(position).getPassengers();
        int id=Integer.parseInt(name.getTag().toString());

        for(int k=0;k<pas.size();k++) {
            if(pas.get(k).getId()==id)
            {//public Students(int id, String name, String image, String category) {
             //   ab=new Students(pas.get(k).getId(),pas.get(k).getName(),pas.get(k).getImage(),pas.get(k).getCategory(),false);

            }

        }
        Log.e("Current position",pos);
        ArrayList<Students>student = new ArrayList<>();
//        student=bean.get(Integer.parseInt(pos)).getStud();
        student.add(ab);
        for(int j = 0; j < absent.size(); j++)
        {
            Students obj = absent.get(j);

            if(obj.getId()==id){
                //found, delete.
                absent.remove(j);
                break;
            }
        }
//public Students(int id, String name, String image, String category) {
       /* attendance model = new attendance(bean.get(Integer.parseInt(pos)).getArea(), "true", student,absent);*/
       /* Students model=new Students(bean.get(position).getPassengers().get(Integer.parseInt(pos)).getId(),bean.get(position).getPassengers().get(Integer.parseInt(pos)).getName(),
                bean.get(position).getPassengers().get(Integer.parseInt(pos)).getImage(),bean.get(position).getPassengers().get(Integer.parseInt(pos)).getCategory(),false);

       *//* bean_absent.set(Integer.parseInt(pos),model)*//*
        absent.set(Integer.parseInt(pos),model);*/
        notifyDataSetChanged();
    }

    @Override
    public void onItemLongClick(int position, View v) {

    }
});

 /* viewHolder.status12.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {

                for(int i=0;i<absent.size();i++)
                {
                    Log.e("Student_id",absent.get(i).getId()+"");
                    Log.e("Student_Name",absent.get(i).getName()+"");
                    Log.e("Student_Image",absent.get(i).getImage()+"");
                    Log.e("Student_Category",absent.get(i).getCategory()+"");
                }
                Log.e("length of present", present.size() + "");
                Log.e("Length of stud", bean.get(position).getPassengers().size() + "");
                Log.e("current possition", view.getTag().toString());

              //  if (bean.get(position).getFlag().equals("true")) {
                    if (Integer.parseInt(view.getTag().toString()) != bean.size()) {
                        for (int i = 0; i < bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().size(); i++) {
                            int stud_id = bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getId();

                            Boolean matched = true;
                           for (int j = 0; j < present.size(); j++) {
                                int pre_stud_id = present.get(j).getId();
                                if (pre_stud_id==stud_id) {
                                    matched = false;
                                }
                            }
                            if (matched) {
                                //public Students(int id, String name, String image, String category) {
                               *//* Boolean status = bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i).status;
                                if (!status)
                                    absent.add(bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i));
                               *//* int id=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getId();
                                String name=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getName();
                                String category=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getCategory();
                                String Image=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getImage();
                                Students ab=new Students(id,name,Image,category);
                                absent.add(ab);
                            }
                        }

                        bean_absent=absent;
//Add student in current area
*//*                        ArrayList<Students> student = new ArrayList<>();
                        ArrayList<Students> abdelete = new ArrayList<>();
                        student = bean.get(Integer.parseInt(view.getTag().toString())).getStud();
                        attendance model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())).getArea(), "true", student, abdelete);

                        bean.set(Integer.parseInt(view.getTag().toString()), model);*//*
                        notifyDataSetChanged();
//Add Absent Student in Next Area

                        *//*student = bean.get(Integer.parseInt(view.getTag().toString()) + 1).getStud();
                        model = new attendance(bean.get(Integer.parseInt(view.getTag().toString()) + 1).getArea(), "true", student, absent);

                        bean.set(Integer.parseInt(view.getTag().toString()) + 1, model);
                        notifyDataSetChanged();
                        // abgridViewAdapter.notifyDataSetChanged();
                        bean.get(position).setFlag("false");
                        if(position+1==bean.size())
                        {

                        }
                        else
                        {
                            bean.get(position+1).setFlag("true");
                        }*//*
                    }
//                }
//                else
//                {
//
//                    Toast.makeText(context, "Please follow the route", Toast.LENGTH_SHORT).show();
//                }
                }catch(Exception e)
                {
                    Log.e("error_instatus", e.getMessage().toString());
                }


            }


    });*/
    viewHolder.imgbutn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                Students ab=new Students();
                for(int i=0;i<absent.size();i++)
                {
                    Log.e("Student_id",absent.get(i).getId()+"");
                    Log.e("Student_Name",absent.get(i).getName()+"");
                    Log.e("Student_Image",absent.get(i).getImage()+"");
                    Log.e("Student_Category",absent.get(i).getCategory()+"");
                }
                Log.e("length of present", present.size() + "");
                Log.e("Length of stud", bean.get(position).getPassengers().size() + "");
                Log.e("current possition", view.getTag().toString());

                //  if (bean.get(position).getFlag().equals("true")) {
                if (Integer.parseInt(view.getTag().toString()) != bean.size()) {
                    for (int i = 0; i < bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().size(); i++) {
                        int stud_id = bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getId();

                        Boolean matched = true;
                        for (int j = 0; j < present.size(); j++) {
                            int pre_stud_id = present.get(j).getId();
                            if (pre_stud_id==stud_id) {
                                matched = false;
                            }
                        }
                        if (matched) {
                            //public Students(int id, String name, String image, String category) {
                               /* Boolean status = bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i).status;
                                if (!status)
                                    absent.add(bean.get(Integer.parseInt(view.getTag().toString())).getStud().get(i));
                               */ int id=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getId();
                            String name=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getName();
                            String category=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getCategory();
                            String Image=bean.get(Integer.parseInt(view.getTag().toString())).getPassengers().get(i).getImage();
                          /*  ab=new Students(id,name,Image,category,false);*/
                           // absent.add(ab);
                        }
                    }

                    /*bean_absent=absent;*/

                    if(Integer.parseInt(view.getTag().toString())==bean.size())
                       bean_absent.add(Integer.parseInt(view.getTag().toString()), ab);
                    else
                        bean_absent.add(Integer.parseInt(view.getTag().toString())+1, ab);
                        //bean_absent.set(Integer.parseInt(view.getTag().toString())+1, ab);
                  //  bean_absent.add(Integer.parseInt(view.getTag().toString())+1, ab);
//Add student in current area
/*                        ArrayList<Students> student = new ArrayList<>();
                        ArrayList<Students> abdelete = new ArrayList<>();
                        student = bean.get(Integer.parseInt(view.getTag().toString())).getStud();
                        attendance model = new attendance(bean.get(Integer.parseInt(view.getTag().toString())).getArea(), "true", student, abdelete);

                        bean.set(Integer.parseInt(view.getTag().toString()), model);*/
                    notifyDataSetChanged();
//Add Absent Student in Next Area

                        /*student = bean.get(Integer.parseInt(view.getTag().toString()) + 1).getStud();
                        model = new attendance(bean.get(Integer.parseInt(view.getTag().toString()) + 1).getArea(), "true", student, absent);

                        bean.set(Integer.parseInt(view.getTag().toString()) + 1, model);
                        notifyDataSetChanged();
                        // abgridViewAdapter.notifyDataSetChanged();
                        bean.get(position).setFlag("false");
                        if(position+1==bean.size())
                        {

                        }
                        else
                        {
                            bean.get(position+1).setFlag("true");
                        }*/
                }
//                }
//                else
//                {
//
//                    Toast.makeText(context, "Please follow the route", Toast.LENGTH_SHORT).show();
//                }
            }catch(Exception e)
            {
                Log.e("error_instatus", e.getMessage().toString());
            }
        }
    });

}catch(Exception e )
{
    Log.e("Error msg",e.getMessage());

}
       return convertView;
   }


    private class ViewHolder{

      private TextView area,time;
      private RecyclerView attend,absent;
      private ImageView status12;
      private TimelineView mTimelineView;
      private ImageButton imgbutn;

  }


}

        //  }
        // }

//}}

