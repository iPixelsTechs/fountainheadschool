package kinfo.darshit.dsp.f_school;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kinfo.darshit.dsp.f_school.Model.Route;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Choose_route extends Activity {
Spinner dialog_sp;
    String token;
    ArrayList<Route.Datum> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_route);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        dialog_sp=(Spinner)findViewById(R.id.area);

        token = sp.getString("token", "");
        jsondetails();




    }
    private void jsondetails()
    {
        final ProgressDialog mProgressDialog=new ProgressDialog(Choose_route.this);
        mProgressDialog.setTitle("getting data");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        /*builder = new AlertDialog.Builder(Students_2.this, R.style.AppCompatAlertDialogStyle);
        builder.setView(getLayoutInflater().inflate(R.layout.food_dialog,null));
        builder.setCancelable(false);
        ad = builder.show();
*/

        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl(common.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice=retrofit.create(login.class);



        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        map.put("Authorization",token);
        //Route food=new Route();
        //food.setFoodInfoId(Integer.parseInt(CoffeeAreaID));


        //Call<List<Route>> call=loginservice.GetTimeSlots(map);
        Call<Route> call = loginservice.GetTimeSlots(map);
        //call.enqueue(new Callback<List<Route>>() {
        call.enqueue(new Callback<Route>() {
            @Override
            public void onResponse(Call<Route> call, Response<Route> response) {
                Log.e("Result",response.body().getMessage().toString());
                data=response.body().getData();
                ArrayList<String> code=new ArrayList<String>();
                for(int i=0;i<data.size();i++)
                {
                    code.add(data.get(i).getCode());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Choose_route.this, android.R.layout.simple_spinner_item, code);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                dialog_sp.setAdapter(adapter);
                mProgressDialog.dismiss();


            }

            @Override
            public void onFailure(Call<Route> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });


    }

}
