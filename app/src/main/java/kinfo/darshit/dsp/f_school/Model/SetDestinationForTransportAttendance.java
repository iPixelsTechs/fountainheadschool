package kinfo.darshit.dsp.f_school.Model;

/**
 * Created by COMP on 07-02-2018.
 */

public class SetDestinationForTransportAttendance {
      int attendanceId;
      String routeDestinationTime;

    public int getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(int attendanceId) {
        this.attendanceId = attendanceId;
    }

    public String getRouteDestinationTime() {
        return routeDestinationTime;
    }

    public void setRouteDestinationTime(String routeDestinationTime) {
        this.routeDestinationTime = routeDestinationTime;
    }
}
