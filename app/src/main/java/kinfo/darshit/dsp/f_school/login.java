package kinfo.darshit.dsp.f_school;



import android.util.Log;

import org.json.JSONObject;

import java.util.Map;

import kinfo.darshit.dsp.f_school.Model.Attendance_Result;
import kinfo.darshit.dsp.f_school.Model.Bulk_stud;
import kinfo.darshit.dsp.f_school.Model.GetRoute;
import kinfo.darshit.dsp.f_school.Model.Route;
import kinfo.darshit.dsp.f_school.Model.SetDestinationForTransportAttendance;
import kinfo.darshit.dsp.f_school.Model.Student_list;
import kinfo.darshit.dsp.f_school.Model.TUpdateDateTimeDetails;
import kinfo.darshit.dsp.f_school.Model.send_attendance;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by COMP on 27-03-2017.
 */

public interface login {

    @Headers("Content-Type: application/json")
    @POST("Login")
    Call<login_model> login(@Body login_model login);

    @GET("GetTimeSlots")
    Call<Route> GetTimeSlots(@HeaderMap Map<String, String> headers);

    @GET("GetRoutes?")
    Call<GetRoute> getroute(@HeaderMap Map<String, String> headers, @Query("timeSlotId") String apiKey);

    @GET("GetStudentsByRoute?")
    Call<Student_list> getStudent(@HeaderMap Map<String, String> headers, @Query("routeId") int apiKey);

    @GET("GetStudentsForSearch")
    Call<Bulk_stud> Insert_bulk_stud(@HeaderMap Map<String, String> headers);

    @POST("TransportAttendance")
    Call<send_attendance> send_data(@HeaderMap Map<String, String> headers, @Body send_attendance data);

//Attendance_Result

    @POST("TransportAttendance")
    Call<Attendance_Result> send_data_istofs(@HeaderMap Map<String, String> headers, @Body send_attendance data);

    @POST("TransportAttandanceUpdateDateTime")
    Call<Attendance_Result> send_data_istofs2(@HeaderMap Map<String, String> headers, @Body TUpdateDateTimeDetails data);

    //SetDestinationForTransportAttendance

    @POST("SetDestinationForTransportAttendance")
    Call<SetDestinationForTransportAttendance> setDestination_transport(@HeaderMap Map<String, String> headers, @Body SetDestinationForTransportAttendance data);



}
