package kinfo.darshit.dsp.f_school;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import kinfo.darshit.dsp.f_school.Attendance_adapter.PlanetAdapter;
import kinfo.darshit.dsp.f_school.Database.Database;
import kinfo.darshit.dsp.f_school.Model.Stud_Data;

public class Search extends AppCompatActivity {
    // List view
    private ListView lv;

    // Listview Adapter
    //ArrayAdapter<String> adapter;

    // Search EditText
    EditText inputSearch;

/*    String products[] = {"Dell Inspiron", "HTC One X", "HTC Wildfire S", "HTC Sense", "HTC Sensation XE",
            "iPhone 4S", "Samsung Galaxy Note 800",
            "Samsung Galaxy S3", "MacBook Air", "Mac Mini", "MacBook Pro"};*/
String products[] ;
    ArrayList<String>std_name;
    ArrayList<Stud_Data>bulk_studs;
    PlanetAdapter adapter;
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_search);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        final Database db = new Database(getApplicationContext(), "table", null, 1);
        db.Opendb();
        Cursor cr = db.get_Stude();
        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        if (cr.getCount() > 0) {
            /*sync_data.setImageResource(R.drawable.ic_history_black_24dp);
            */cr.moveToFirst();
            /*data_found=1;*/
            bulk_studs=new ArrayList<>();
            std_name=new ArrayList<>();
            do{

                int id=Integer.parseInt(cr.getString(cr.getColumnIndex("sid")));
                String Name=cr.getString(cr.getColumnIndex("name"));
                String section=cr.getString(cr.getColumnIndex("sections"));
                String grade=cr.getString(cr.getColumnIndex("grade"));
                String img=cr.getString(cr.getColumnIndex("image"));
                String category=cr.getString(cr.getColumnIndex("category"));
                String EmergencyContactNo = cr.getString(cr.getColumnIndex("emergencyContactNo"));
                String FskId = cr.getString(cr.getColumnIndex("fskId"));
                String HrisEmployeeID = cr.getString(cr.getColumnIndex("hrisEmployeeID"));
                String StopName = cr.getString(cr.getColumnIndex("stopName"));
                //String emergencyContactNo, String fskId, String hrisEmployeeID) {
                String stud=Name + " (" + grade + ")" + " section :" + section+" Category :"+category;
                Stud_Data data = new Stud_Data(category, id, Name, grade, section, img,EmergencyContactNo,FskId,HrisEmployeeID,stud,StopName);


                Log.e("Name",Name+"---"+category);
                if(category.equals("Student")) {
                    std_name.add(Name + " (" + grade + ")" + " section :" + section);
                }
                else
                {
                    std_name.add(Name + " (" + category + ")" );
                }
                bulk_studs.add(data);
            }while (cr.moveToNext());
            db.close();
            adapter = new PlanetAdapter(bulk_studs,Search.this,Search.this);
            lv.setAdapter(adapter);

        }
        else {
            //sync_data.setImageResource(R.drawable.ic_sync_problem_black_24dp);
        }

        // Adding items to listview

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                System.out.println("Text ["+cs+"] - Start ["+arg1+"] - Before ["+arg2+"] - Count ["+arg3+"]");
                if (arg3 < arg2) {
                    // We're deleting char so we need to reset the adapter data
                    adapter.resetData();
                }

                adapter.getFilter().filter(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
             }

            @Override
            public void afterTextChanged(Editable arg0) {


            }
        });
    }


    }


