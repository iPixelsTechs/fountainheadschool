package kinfo.darshit.dsp.f_school.Model;

import android.util.Log;

/**
 * Created by COMP on 14-07-2017.
 */

public class Report_data {
    String Date,create_Date,send_date,Route,isSend;

    int id;


    public Report_data() {
    }

    public Report_data(String date, String create_Date, String date1, String route, String isSend, int id) {
        Date = date;
        this.create_Date = create_Date;
        this.send_date = date1;
        Route = route;
        this.isSend = isSend;
        this.id = id;
    }

    public String getSend_date() {
        return send_date;
    }

    public void setSend_date(String send_date) {
        this.send_date = send_date;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getRoute() {
        return Route;
    }

    public void setRoute(String route) {
        Route = route;
    }

    public String getIsSend() {
        return isSend;
    }

    public void setIsSend(String isSend) {
        this.isSend = isSend;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(String create_Date) {
        this.create_Date = create_Date;
    }
}
