package kinfo.darshit.dsp.f_school;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class spalsh extends AppCompatActivity {
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    String mobile,pwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_spalsh);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ed = sp.edit();
        String Login_Status = sp.getString("token", "");
        Log.e("Login_status", Login_Status);
        if (Login_Status.isEmpty()) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        } else {
            mobile = sp.getString("mobile", "");
            pwd = sp.getString("pwd", "");

      /*    RestAPI api=new RestAPI();
            JSONObject json=new JSONObject();
                  try{
                      json=api.Login("9033063009","123456");
                  }catch (Exception e)
                  {
                      Log.e("error",e.getMessage());
                  }
*/


             jsonlogin();

        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
    private void jsonlogin() {
        final ProgressDialog mProgressDialog=new ProgressDialog(spalsh.this);
        mProgressDialog.setTitle("getting data");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        /*mProgressDialog.show();
*/


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(common.url2)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        login loginservice = retrofit.create(login.class);

        login_model log = new login_model();
        /*log.setMobileNo("9033063009");
        log.setPassword("123456");
        */log.setMobileNo(mobile);
        log.setPassword(pwd);
        Call<login_model> call = loginservice.login(log);

        call.enqueue(new Callback<login_model>() {
            @Override
            public void onResponse(Call<login_model> call, Response<login_model> response) {
                try {
                    Log.e("responce", "------------------------------------------");
                    Log.e("responce", response.body().getMessage());
                    //     Log.e("data",response.body().getData().getAuthToken().toString() );
                    if (response.body().getMessage().equals("Success")) {
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(spalsh.this);
                        SharedPreferences.Editor e = sp.edit();
                        e.putString("token", response.body().getData().getAuthToken());
                        e.putString("userid", response.body().getData().getUserId() + "");
                        e.putString("mobile",mobile);
                        e.putString("pwd",pwd);
                        e.commit();
                        Intent i = new Intent(getApplication(), Students_2.class);
                        startActivity(i);
                        Log.e("Token", response.body().getData().getAuthToken());
                     /*   mProgressDialog.dismiss();*/
                        finish();
                    } else {
                       /* mProgressDialog.dismiss();*/
                        Toast.makeText(spalsh.this, "Invalid Userid or password", Toast.LENGTH_LONG).show();
                    }
                    Log.e("responce", "------------------------------------------");
                }catch(Exception e)
                {
                    Toast.makeText(getApplicationContext(),"Wrong userid or password",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<login_model> call, Throwable t) {
                Log.e("responce", "------------------------------------------");
                Log.e("Fail", t.getMessage());
                Log.e("responce", "------------------------------------------");
                Toast.makeText(getApplicationContext(),"check your internet connection.",Toast.LENGTH_LONG).show();
               /* mProgressDialog.dismiss();*/
            }
        });

    }

}
